#!/bin/sh
export GO111MODULE=auto
go get github.com/mostlygeek/arp github.com/fyne-io/systray github.com/getlantern/systray golang.org/x/sync/semaphore github.com/donomii/go-imap github.com/mattn/go-shellwords github.com/schollz/closestmatch github.com/atotto/clipboard github.com/emersion/go-autostart  github.com/emersion/go-sasl github.com/google/uuid github.com/fyne-io/systray gitlab.com/donomii/wasm-lsystem/lsystem github.com/lucor/systray
go get "github.com/donomii/glim" "github.com/donomii/goof" "golang.org/x/crypto" "github.com/go-gl/glfw/v3.3/glfw" "github.com/go-gl/gl/v2.1/gl" "github.com/agnivade/levenshtein" github.com/donomii/sceneCamera github.com/go-gl/mathgl/mgl32 github.com/google/uuid github.com/srwiley/rasterx github.com/srwiley/oksvg  github.com/google/uuid github.com/fyne-io/systray
mkdir build
cd build
rm universal_menu_main.exe universal_menu_command_line_toggle universal_menu_hotkey_monitor universal_menu_launcher.exe universal_menu_launcher.exe KeyTap_mac universal_menu_tray.exe universal_menu_floaty.exe universal_menu_tray
cd ..
go build -o build/universal_menu_launcher.exe ./launcher
go build -o build/universal_menu_launcher ./launcher
go build -o build/universal_menu_main.exe ./umm
go build -o build/universal_menu_floaty.exe ./floaty2
go build -o build/universal_menu_tray.exe ./tray
go build -o build/universal_menu_tray ./tray
go build -o build/universal_menu_headless.exe ./trayheadless
go build -o build/frogpond-server ./frogpond/apps/server/
go build -o build/frogpond ./frogpond/apps/client/
go build -ldflags="-s -w" -o build/universal_menu_command_line_toggle helpers/command_line_toggle.go
gcc -Ofast -Wall -o mac_launcher helpers/macGlobalKeyHook.c  -framework ApplicationServices -o build/universal_menu_hotkey_monitor
gcc -Ofast -Wall -o mac_launcher helpers/KeyTap_mac.c  -framework ApplicationServices -o build/KeyTap_mac
gcc -Ofast -Wall -o mac_launcher helpers/KeyTap_mac.c  -framework ApplicationServices -o build/universal_key_tap.exe
cp -r icon build/
ln -f build/* traymenu/
mkdir ~/.umh
cp -rn tray/config_examples traymenu/
cp -rn tray/config_examples ~/.umh
tar -czvf traymenu.tar.gz traymenu
ls -la traymenu/
cd p2p
sh build.sh
cd ..
cp p2p/build/* traymenu/
