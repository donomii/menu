#!/bin/sh
export GO111MODULE=auto
export BUILDDIR=build/funzone-$(uname)-$(uname -m)/
rm -r build || true
mkdir -p $BUILDDIR

#sudo apt-get install libegl1-mesa-dev libgles2-mesa-dev libx11-dev
#sudo nala install libayatana-appindicator3-dev




go get github.com/mostlygeek/arp github.com/getlantern/systray golang.org/x/sync/semaphore github.com/emersion/go-imap github.com/mattn/go-shellwords github.com/schollz/closestmatch github.com/atotto/clipboard github.com/emersion/go-autostart  github.com/emersion/go-sasl github.com/srwiley/oksvg github.com/srwiley/rasterx github.com/go-gl/mathgl/mgl32 gitlab.com/donomii/sceneCamera gitlab.com/donomii/wasm-lsystem/lsystem github.com/fyne-io/systray
go get "github.com/donomii/glim" "github.com/donomii/goof" "golang.org/x/crypto" "github.com/go-gl/glfw/v3.3/glfw" "github.com/go-gl/gl/v2.1/gl" "github.com/agnivade/levenshtein"
mkdir $BUILDDIR
mkdir traymenu

cp -rn tray/config_examples $BUILDDIR

cd $BUILDDIR
rm universal_menu_main.exe universal_menu_command_line_toggle universal_menu_hotkey_monitor universal_menu_launcher.exe KeyTap_mac universal_menu_tray.exe universal_menu_floaty.exe traymenu-$(uname)-$(uname -m).tar.gz
cd ../..
go build -o $BUILDDIR/universal_menu_launcher.exe ./launcher
go build -o $BUILDDIR/universal_menu_main.exe ./umm
go build -o $BUILDDIR/universal_menu_floaty.exe ./floaty2
go build -o $BUILDDIR/universal_menu_tray.exe ./tray
go build -o $BUILDDIR/universal_menu_headless.exe ./trayheadless
go build -ldflags="-s -w" -o $BUILDDIR/universal_menu_command_line_toggle helpers/command_line_toggle.go
cp -r icon $BUILDDIR/
cp -r $BUILDDIR/* traymenu/
mkdir ~/.umh
tar -czvf traymenu-$(uname)-$(uname -m).tar.gz traymenu
cp traymenu-$(uname)-$(uname -m).tar.gz traymenu
ls -la traymenu/
