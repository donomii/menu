from padatious.intent_container import IntentContainer
import json
import os
import sys

# List all files in current directory that end in .intent
intent_files = [f for f in os.listdir('.') if f.endswith('.intent')]

print(intent_files)

container = IntentContainer('cache')
# Load all intent files into the container
for intent_file in intent_files:
    # Remove the .intent extension from the file name
    intent_name = intent_file[:-7]
    print("Loading intent: " + intent_name + " from file: " + intent_file)
    container.load_file(intent_name, intent_file)


container.train(single_thread=True)

# Read lines from STDIN in a loop
while True:
    wholeline = input()
    line = wholeline
    # Remove prefix from line
    if line.startswith("partial: "):
        line = line[9:]
    if line.startswith("complete: "):
        line = line[10:]
    data = container.calc_intent(line)
    sent = data.sent
    # if sent is not an array
    if not isinstance(sent, list):
        sent = [sent]

    out = {"name": data.name, "sent": sent,
           "matches": data.matches, "conf": data.conf,
           "input": wholeline}
    # Serialise as json string
    outstr = json.dumps(out)
    print(outstr)
    # Flush output buffers
    sys.stdout.flush()
    sys.stderr.flush()
