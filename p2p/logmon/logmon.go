package main

import (
	"flag"
	"os"

	p2p ".."
	"fmt"
    "log"
    "path/filepath"
    "runtime"

    "github.com/fsnotify/fsnotify"
)

func getHostName() string {
	// Get the hostname
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	return hostname
}




func fileExists(filename string) bool {
	_, err := os.Stat(filename)
	return !os.IsNotExist(err)
}

func getLogFile() (string, error) {
	var logFile string

	switch runtime.GOOS {
	case "linux":
		systemdLogFile := "/var/log/journal" // For systemd
		syslogLogFile := "/var/log/syslog"    // For syslog

		if fileExists(systemdLogFile) {
			logFile = systemdLogFile
		} else if fileExists(syslogLogFile) {
			logFile = syslogLogFile
		} else {
			return "", fmt.Errorf("unable to find log file")
		}
	case "darwin":
		logFile = "/var/log/system.log"
	case "windows":
		logFile = "C:\\Windows\\System32\\winevt\\Logs\\System.evtx"
	default:
		return "", fmt.Errorf("unsupported platform")
	}

	return logFile, nil
}
func monitorSystemLog(callback func(string)) error {
	logFile, err := getLogFile()
	if err != nil {
		return err
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	defer watcher.Close()

	done := make(chan bool)
	lastReadPosition := int64(0)

	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Op&fsnotify.Write == fsnotify.Write {
					log.Println("Log file modified:", event.Name)

					file, err := os.Open(event.Name)
					if err != nil {
						log.Println("Error opening log file:", err)
						continue
					}

					fileInfo, err := file.Stat()
					if err != nil {
						log.Println("Error getting file info:", err)
						file.Close()
						continue
					}

					newSize := fileInfo.Size()

					if newSize < lastReadPosition {
						lastReadPosition = 0
					}

					deltaSize := newSize - lastReadPosition
					data := make([]byte, deltaSize)

					_, err = file.ReadAt(data, lastReadPosition)
					file.Close()

					if err != nil {
						log.Println("Error reading log file:", err)
						continue
					}

					callback(string(data))
					lastReadPosition = newSize
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("Error:", err)
			}
		}
	}()

	logFileDir := filepath.Dir(logFile)
	err = watcher.Add(logFileDir)
	if err != nil {
		return err
	}
	<-done
	return nil
}

func main() {

	// Command line options
	// -bind <ip:port>  Listen on this address
	var bindAddr string
	flag.StringVar(&bindAddr, "bind", ":16171", "Listen address")
	var peerAddr string
	flag.StringVar(&peerAddr, "peer", "", "Startup peer address")
	var SecretNetworkPhrase string
	flag.StringVar(&SecretNetworkPhrase, "secret", "", "Secret network phrase")
	var password string
	flag.StringVar(&password, "password", "", "Password")
	var nodeName string = getHostName()
	flag.StringVar(&nodeName, "name", nodeName, "Node name")
	flag.BoolVar(&p2p.ExtraDebug, "extradebug", false, "Extra debug")
	flag.BoolVar(&p2p.GlobalDebug, "debug", false, "debug")
	flag.Parse()

	p, err := p2p.NewNetwork(bindAddr, []byte(SecretNetworkPhrase), []byte(password))
	if err != nil {
		panic(err)
	}
	p.LoadDefaultConfig(nodeName)
	p.AddEndpoint(peerAddr)

	p.NodeName = nodeName

	p.SaveDefaultConfig(nodeName)

	go p.Start()
	
	monitorSystemLog(func(logEntry string) {
		p.Broadcast("log", []byte(logEntry))
		p.SaveDefaultConfig(nodeName)
	})
	

}
