package main

//This is a command line client for a long runner server connected to a p2p network. It connects to the server to retrieve information.  The p2p service shares a list of services.  Each node maintains its own copy.  This client can search for services by name, using the command line "p2p service <service name>".  This client then connects to the server on the default port (8999) and calls the REST endpoint /v1/service/<service name> to retrieve the service information.

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"encoding/base64"
	"encoding/json"

	p2p ".."
	frogpond "../../frogpond"
)

func main() {

	// Command line options

	// http server port
	var httpPort string
	flag.StringVar(&httpPort, "http-port", "8999", "HTTP port")
	var wantJson bool
	flag.BoolVar(&wantJson, "json", false, "Print JSON")
	flag.Parse()

	// Parse command line to get service name
	if len(flag.Args()) < 1 {
		printHelp()
		os.Exit(1)
	}

	operation := flag.Args()[0]

	// Get service or node name
	var name string
	if operation == "service" || operation == "node" || operation == "search" || operation == "file" || operation == "add" || operation == "setCode" {
		name = flag.Args()[1]
	}

	switch operation {
	case "service":
		url := fmt.Sprintf("http://localhost:%v/v1/service/%s", httpPort, name)
		serviceAnnouncement, err := get[p2p.ServiceAnnouncement](url)
		if err != nil {
			panic(err)
		}

		if wantJson {

			// Print service
			json.NewEncoder(os.Stdout).Encode(serviceAnnouncement)
		} else {

			fmt.Printf("%v://%v:%v", serviceAnnouncement.ServiceProtocol, serviceAnnouncement.ServiceAddress, serviceAnnouncement.ServicePort)
		}
	case "services":
		url := fmt.Sprintf("http://localhost:%v/v1/services", httpPort)
		serviceAnnouncements, err := get[[]p2p.ServiceAnnouncement](url)
		if err != nil {
			panic(err)
		}

		if wantJson {
			json.NewEncoder(os.Stdout).Encode(serviceAnnouncements)
		} else {
			for _, serviceAnnouncement := range *serviceAnnouncements {
				fmt.Printf("%v://%v:%v\n", serviceAnnouncement.ServiceProtocol, serviceAnnouncement.ServiceAddress, serviceAnnouncement.ServicePort)
			}
		}

	case "node":
		url := fmt.Sprintf("http://localhost:%v/v1/node/%s", httpPort, name)

		nodeAnnouncement, err := get[p2p.Announcement](url)
		if err != nil {
			panic(err)
		}

		if wantJson {
			json.NewEncoder(os.Stdout).Encode(nodeAnnouncement)
		} else {
			fmt.Printf("%v:\n", nodeAnnouncement.Name)
			for _, ip := range nodeAnnouncement.KnownIps {
				fmt.Printf("	%v\n", ip)
			}
			fmt.Printf("\n")
		}

	case "nodes":
		url := fmt.Sprintf("http://localhost:%v/v1/nodes", httpPort)
		nodeAnnouncements, err := get[[]p2p.Announcement](url)
		if err != nil {
			log.Printf("Error: %v", err)
			panic(err)
		}

		if wantJson {
			json.NewEncoder(os.Stdout).Encode(nodeAnnouncements)
		} else {
			for _, nodeAnnouncement := range *nodeAnnouncements {
				fmt.Printf("%v\n", nodeAnnouncement.Name)
			}
		}
	
	case "search":
		url := fmt.Sprintf("http://localhost:%v/v1/search/%s", httpPort, name)
		log.Println(url)
		searchResults, err := get[[]p2p.SearchResult](url)
		if err != nil {
			log.Printf("Error: %v", err)
			panic(err)
		}
		if wantJson {
			json.NewEncoder(os.Stdout).Encode(searchResults)
		} else {
			for _, searchResult := range *searchResults {
				fmt.Printf("%v\n", searchResult.Name)
			}
		}
	case "file":
		//Encode with base64 to stop the stupid golang http server from ficking up the url
		encoded_name := base64.URLEncoding.EncodeToString([]byte(name))
		url := fmt.Sprintf("http://localhost:%v/v1/file/%s", httpPort, encoded_name)
		log.Println(url)
		//Download file and save it, using the filename from the header
		response, err := http.Get(url)
		if err != nil {
			log.Printf("Error: %v", err)
			panic(err)
		}
		defer response.Body.Close()
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Printf("Error: %v", err)
			panic(err)
		}
		filename := response.Header.Get("Content-Disposition")
		fmt.Println(filename)

		// Save file
		os.WriteFile(filename, body, 0644)
	case "add":
		url := fmt.Sprintf("http://localhost:%v/v1/addpeer/%s", httpPort, name)
		log.Println(url)
		http.Get(url)
		fmt.Printf("Added %v to local node\n", name)
	case "secret":
		url := fmt.Sprintf("http://localhost:%v/v1/secret", httpPort)
		log.Println(url)
		secret, err := getRaw(url)
		if err != nil {
			log.Printf("Error: %v", err)
			panic(err)
		}
		fmt.Printf("Secret: %v\n", string(secret))
		case "peers":
		url := fmt.Sprintf("http://localhost:%v/v1/peers", httpPort)
		log.Println(url)
		type Peer struct {
			p2p.Node
			Name   string
			Ips    []string
			LinkIp string
		}
		var peers  *[]Peer
		peers, err := get[[]Peer](url)
		if err != nil {
			log.Printf("Error: %v", err)
			panic(err)
		}
		if wantJson {
			fmt.Println(peers)
		} else {
			for _, peer := range *peers {
				fmt.Printf("%v: %v\n",peer.LinkIp, peer.Name)
			}
		}

	case "set":
		key := os.Args[2]
		value := os.Args[3]
		//Encode with base64 to stop the stupid golang http server from ficking up the url
		encoded_key := base64.URLEncoding.EncodeToString([]byte(key))
		encoded_value := base64.URLEncoding.EncodeToString([]byte(value))
		url := fmt.Sprintf("http://localhost:%v/v1/setdatapoint/%s/%s", httpPort, encoded_key, encoded_value)
		log.Println(url)
		fmt.Println(key,":",value)
		http.Get(url)
		fmt.Printf("Set %v to %v\n", key, value)
		
	case "get":
		key := os.Args[2]
		//Encode with base64 to stop the stupid golang http server from ficking up the url
		encoded_key := base64.URLEncoding.EncodeToString([]byte(key))
		url := fmt.Sprintf("http://localhost:%v/v1/getdatapoint/%s", httpPort, encoded_key)
		log.Println(url)
		jsonBytes, err := getRaw(url)
		if err != nil {
			log.Printf("Error: %v", err)
			panic(err)
		}
		if wantJson {
			fmt.Printf("%v\n", string(jsonBytes))
			return
		}
		var datapoints []frogpond.DataPoint
		err = json.Unmarshal(jsonBytes, &datapoints)
		if err != nil {
			log.Printf("Error: %v while processing %v", err, string(jsonBytes))
			panic(err)
		}

		for _, datapoint := range datapoints {
			fmt.Printf("%v\n", string(datapoint.Value))
		}

		case "announce":
		url := fmt.Sprintf("http://localhost:%v/v1/announce", httpPort)
		log.Println(url)
		http.Get(url)
		fmt.Printf("Announced\n")

		case "notify":
			message := os.Args[2:]
			//Encode with base64 to stop the stupid golang http server from ficking up the url
			encoded_message := base64.URLEncoding.EncodeToString([]byte(strings.Join( message, " " )))
			url := fmt.Sprintf("http://localhost:%v/v1/notify/%s", httpPort, encoded_message)
			log.Println(url)
			http.Get(url)
			fmt.Printf("Notified\n")


		
	default:
		printHelp()
		os.Exit(1)
	}

}

// Use generics to fetch data from a url, and unmarshal it into a struct
func get[T any](url string) (*T, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var data T
	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Printf("Error: %v unmarshalling %v", err, string(body))
		return nil, err
	}

	return &data, nil
}

func getRaw(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	return ioutil.ReadAll(response.Body)
}

func printHelp() {
	fmt.Println("Usage: p2p -http-port <port> command opts")
	fmt.Println("p2p service <service name>   - returns the url of a service")
	fmt.Println("p2p services                 - returns all services")
	fmt.Println("p2p node <node name>         - returns the ip addresses of a node")
	fmt.Println("p2p nodes                    - returns all nodes")
	fmt.Println("p2p search <search string>	  - returns all files that match the search string, from fileserver nodes")
	fmt.Println("p2p file <file name>         - downloads the file from a fileserver node")
	fmt.Println("p2p add <peer address:port>  - adds a peer to the local node")
	fmt.Println("p2p secret                   - returns the secret of the local node")
	fmt.Println("p2p peers                    - returns all the connected peers of the local node")
	fmt.Println("p2p set <key> <value>        - sets a global datapoint")
	fmt.Println("p2p get <key>                - gets a global datapoint")
	fmt.Println("p2p announce                 - announces the local node to the network")
	fmt.Println("p2p notify <message>         - user notification to all connected peers")
}
