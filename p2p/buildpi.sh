#!/bin/sh

go get github.com/fsnotify/fsnotify
go get gitlab.com/donomii/menu/frogpond

mkdir buildpi
export GOOS=linux 
export GOARCH=arm 
export GOARM=5

go build -o buildpi/node ./cli/
go build -o buildpi/fileshare ./fileshare/
go build -o buildpi/advertise ./advertise/
go build -o buildpi/logmon ./logmon/
go build -o buildpi/p2p ./p2p/
