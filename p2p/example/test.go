package main

import (
	"encoding/json"
	"fmt"
	"time"

	p2p ".."
)

type Announcement struct {
	Name     string
	KnownIps []string
}

func main() {

	//Make 5 nodes, on 5 different ports
	//Connect them all to each other
	//Announce every 5 seconds
	var numInstances = 50
	var err error
	testNodes := make([]*p2p.Network, numInstances)
	for i := 0; i < numInstances; i++ {
		testNodes[i], err = p2p.NewNetwork(fmt.Sprintf("0.0.0.0:90%02d", i), []byte("mynetwork"))
		testNodes[i].NodeName = fmt.Sprintf("Node %d", i)
		if err != nil {
			panic(err)
		}
	}

	ha := func(network *p2p.Network, msg *p2p.Message) {
		var ann Announcement = Announcement{}
		json.Unmarshal(msg.Content, &ann)

		fmt.Printf("(%v) Saw announce from %v, %v hops away\n", network.BindAddrPort, ann.Name, msg.Hops)

		for _, ip := range ann.KnownIps {
			network.AddEndpoint(ip)
		}
	}

	for i := 0; i < numInstances; i++ {
		testNodes[i].MsgCallbacks.Store("announce", ha)
		go testNodes[i].Listen()
	}

	for i := 0; i < numInstances; i++ {
		for j := 0; j < numInstances; j++ {
			if i != j {
				go func(i, j int) {
					testNodes[i].Connect(fmt.Sprintf("127.0.0.1:900%d", j))
				}(i, j)
			}
		}
	}

	for {

		fmt.Println("Announcing")

		testNodes[0].Announce(testNodes[0].NodeName, ":16171")
		//Broadcast a ping
		testNodes[0].Broadcast("ping", []byte(""))

		time.Sleep(5 * time.Second)
	}

}
