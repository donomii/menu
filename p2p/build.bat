@echo off

REM Set the GO111MODULE environment variable
set GO111MODULE=auto

REM Get the required package
go get -u gitlab.com/donomii/menu/frogpond

REM Make the build directory
if not exist build mkdir build

REM Build the various components
go build -o build\node.exe .\cli\
go build -o build\fileshare.exe .\fileshare\
go build -o build\advertise.exe .\advertise\
go build -o build\logmon.exe .\logmon\
go build -o build\p2p.exe .\p2p\

pause

