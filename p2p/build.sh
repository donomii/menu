#!/bin/sh

set -x GO111MODULE auto
export GO111MODULE=auto
go get -u gitlab.com/donomii/menu/frogpond  github.com/fsnotify/fsnotify
mkdir build

go build -o build/node ./cli/
go build -o build/fileshare ./fileshare/
go build -o build/advertise ./advertise/
go build -o build/logmon ./logmon/
go build -o build/p2p ./p2p/
go build -o build/notifier ./notifier/
