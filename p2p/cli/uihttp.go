package main

import (
	p2p ".."
	"encoding/json"
	"net/http"
	"time"
)


import "embed"

// content holds our static web server content.
//go:embed ui/*
//go:embed index.html
var content embed.FS


type Node struct {
	ID        string    `json:"id"`
	Name      string    `json:"name"`
	LastSeen  time.Time `json:"lastSeen"`
	IpList    []string  `json:"ips"`
	PrimaryIp string    `json:"primaryIp"`
}

// Assuming you have a method for getting all the nodes
func getNodes() []*Node {
	var out []*Node
	p2pnet.Nodes.Range(func(key, value interface{}) bool {
		node := value.(p2p.Announcement)
		out = append(out, &Node{
			ID:       p2p.KeyShortName([]byte(p2p.EncodeRSAPublicKey(node.PublicKey))),
			Name:     node.Name,
			LastSeen: node.LastSeen,
			IpList:  node.KnownIps,
			PrimaryIp: node.KnownIps[0],
		})
		return true
	})
	return out
}

func handleNodes(w http.ResponseWriter, r *http.Request) {
	nodes := getNodes()

	// convert nodes to JSON
	jsonBytes, err := json.Marshal(nodes)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonBytes)
}


func getServices(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	
	services := p2pnet.ServicesList()
	
	json.NewEncoder(w).Encode(services)
}



func startUI() {
	http.HandleFunc("/nodes", handleNodes)
	http.HandleFunc("/services", getServices)
	//Serve index.html
	http.Handle("/", http.FileServer(http.FS(content)))
	http.ListenAndServe(":9080", nil)
}
