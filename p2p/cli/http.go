//This is a http server allowing access to the p2p network over http.  The first service is /v1/service/<service name> which returns a json object containing the service information.  The second service is /v1/services which returns a json object containing a list of services.

// Path: p2p/cli/http.go

package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strings"

	"time"

	p2p ".."
	"gitlab.com/donomii/menu/frogpond"
)

var p2pnet *p2p.Network
var frogPondNode *frogpond.Node

// Start a http server on the given port
func startHttp(port int) {
	fmt.Println("Starting http server on port", port)
	//Endpoints: service, services, node, nodes
	http.HandleFunc("/v1/service/", serviceHandler)
	http.HandleFunc("/v1/services", servicesHandler)
	http.HandleFunc("/v1/node/", nodeHandler)
	http.HandleFunc("/v1/nodes", nodesHandler)
	http.HandleFunc("/v1/search/", searchHandler)
	http.HandleFunc("/v1/queryfile", queryFileHandler)
	http.HandleFunc("/v1/file/", fileHandler)
	http.HandleFunc("/v1/addpeer/", addPeerHandler)
	http.HandleFunc("/v1/peers", peersHandler)
	http.HandleFunc("/v1/secret", secretHandler)
	http.HandleFunc("/v1/setdatapoint/", setDataPointFunc)
	http.HandleFunc("/v1/getdatapoint/", getDataPointFunc)
	http.HandleFunc("/v1/announce", announceHandler)
	http.HandleFunc("/v1/notify/", notifyHandler)
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}

// Set a frogpond datapoint
func setDataPointFunc(w http.ResponseWriter, r *http.Request) {
	key_encoded := strings.TrimPrefix(r.URL.Path, "/v1/setdatapoint/")
	key_encoded_b64, value_encoded_b64 := strings.Split(key_encoded, "/")[0], strings.Split(key_encoded, "/")[1]

	key, err := base64.StdEncoding.DecodeString(key_encoded_b64)
	if err != nil {
		fmt.Println("Error decoding key", err)
		panic(err)
	}
	value, err := base64.StdEncoding.DecodeString(value_encoded_b64)
	if err != nil {
		fmt.Println("Error decoding value", err)
		panic(err)
	}
	dps := frogPondNode.SetDataPoint(string(key), value)
	content_bytes, err := json.Marshal(dps)
	if err != nil {
		fmt.Println("Error marshalling json", err)
		panic(err)
	}
	p2pnet.Broadcast("frogpond datapoints",  content_bytes)
	fmt.Printf("Set datapoint %s to %s(%v)\n", key, value, string(content_bytes))
}

// Get a frogpond datapoint
func getDataPointFunc(w http.ResponseWriter, r *http.Request) {
	key_encoded := strings.TrimPrefix(r.URL.Path, "/v1/getdatapoint/")
	key, err := base64.StdEncoding.DecodeString(key_encoded)
	dps := frogPondNode.GetAllMatchingPrefix(string(key))
	content_bytes, err := json.Marshal(dps)
	if err != nil {
		fmt.Println("Error marshalling json", err)
	}
	fmt.Println("Sending", string(content_bytes))
	w.Write(content_bytes)
}

// Handle a http request to display the details of a single service
func serviceHandler(w http.ResponseWriter, r *http.Request) {
	serviceName := strings.TrimPrefix(r.URL.Path, "/v1/service/")
	fmt.Println("Looking up service", serviceName)
	ServicesList := p2pnet.ServicesList()
	var found bool
	for _, service := range ServicesList {
		fmt.Println("Comparing", service.ServiceName, serviceName)
		if service.ServiceName == serviceName {
			fmt.Println("Found service", serviceName)
			json.NewEncoder(w).Encode(service)
			found = true
			return
		}
	}
	if !found {
		fmt.Println("Service not found", serviceName)
		http.Error(w, "Service not found", 404)
	}
}

// Handle a http request to list all the currently connected peer nodes
func peersHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Looking up peers")

	type Peer struct {
		p2p.Node
		Name   string
		Ips    []string
		LinkIp string
	}
	var peers []Peer

	//Build a list of connected peers
	p2pnet.ConnectedNodes.Range(func(key_i, value_i interface{}) bool {
		//peer := key_i.(string)
		p := value_i.(*p2p.Node)

		//FIXME look this node up in the nodes list to get ips and names

		peer := Peer{*p, "", []string{}, ""}

		peer.SymmetricKeyReceive = []byte{}
		peer.SymmetricKeySend = []byte{}
		peer.LinkIp = peer.Conn.RemoteAddr().String()

		nodeDetails_i, ok := p2pnet.Nodes.Load(key_i)
		if ok {
			nodeDetails := nodeDetails_i.(p2p.Announcement)
			peer.Name = nodeDetails.Name
			peer.Ips = nodeDetails.KnownIps
		}

		peers = append(peers, peer)
		return true
	})
	json.NewEncoder(w).Encode(peers)
}

// Handle a http request to list all services
func servicesHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Looking up services")
	ServicesList := p2pnet.ServicesList()
	fmt.Println("Found services", ServicesList)
	json.NewEncoder(w).Encode(ServicesList)
}

// Handle a http request to display the details of a single node
func nodeHandler(w http.ResponseWriter, r *http.Request) {
	nodeName := strings.TrimPrefix(r.URL.Path, "/v1/node/")
	fmt.Println("Looking up node", nodeName)
	NodesList := p2pnet.Nodes
	var found bool
	NodesList.Range(func(key_i, value_i interface{}) bool {
		node := value_i.(p2p.Announcement)
		fmt.Println("Comparing", node.Name, nodeName)
		if node.Name == nodeName {
			fmt.Println("Found node", nodeName)
			json.NewEncoder(w).Encode(node)
			found = true
			return false
		}
		return true
	})
	if !found {
		fmt.Println("Node not found", nodeName)
		http.Error(w, "Node not found", 404)
	}
}

// Handle a http request to list all known nodes
func nodesHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Looking up nodes")
	NodesList := p2pnet.Nodes
	//Make an array of announcements
	var nodes []p2p.Announcement
	NodesList.Range(func(key_i, value_i interface{}) bool {
		node := value_i.(p2p.Announcement)
		nodes = append(nodes, node)
		return true
	})
	//Sort into alphabetical order
	sort.Slice(nodes, func(i, j int) bool {
		return nodes[i].Name < nodes[j].Name
	})

	fmt.Println("Found nodes", nodes)
	json.NewEncoder(w).Encode(nodes)
}

// Handle a http request to search all fileservers for a file
func searchHandler(w http.ResponseWriter, r *http.Request) {
	searchString := strings.TrimPrefix(r.URL.Path, "/v1/search/")
	fmt.Println("Searching for", searchString)

	var out []p2p.SearchResult

	//Search result handler
	ha := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("Received reply to message id '%s' from %v\n", msg.ReplyToId, p2p.PublicKeyToId(p2p.DecodeRSAPublicKey(msg.From)))
		fmt.Printf("Got search result from %s %+v (%s)\n", p2p.PublicKeyToId(p2p.DecodeRSAPublicKey(msg.From)), msg, msg.Content)
		var searchResults []string
		err := json.Unmarshal(msg.Content, &searchResults)
		if err != nil {
			fmt.Printf("Error unmarshalling search results content, %v, '%v'\n", err, string(msg.Content))
		}
		for _, res := range searchResults {
			fmt.Printf("Found %s \n", res)
			out = append(out, p2p.SearchResult{Name: res, Node: []byte(msg.From)})
		}
	}

	msgId := p2pnet.Broadcast("search request", []byte(searchString))
	p2pnet.MessageIdCallbacks.Store(msgId, ha)
	fmt.Println("Waiting for search results for id", msgId)
	time.Sleep(15 * time.Second)
	p2pnet.MessageIdCallbacks.Delete(msgId)
	fmt.Printf("Search results: %v items", len(out))
	json.NewEncoder(w).Encode(out)

}

// why does this exist?
func queryFileHandler(w http.ResponseWriter, r *http.Request) {

	encoded_path := strings.TrimPrefix(r.URL.Path, "/v1/queryfile/")
	fmt.Printf("Path: %s\n", r.URL.Path)
	fmt.Printf("Raw path: %s\n", r.URL.RawPath)
	fmt.Println("Looking up file", encoded_path)
	//Get query param q
	path := r.URL.Query()["q"][0]

	fmt.Println("Requesting file", path)

	fileName := strings.Split(path, "/")[len(strings.Split(path, "/"))-1]
	fmt.Println("Waiting for file", fileName)

	var done bool
	//File result handler
	frh := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("Got file result from %s %+v (%s)\n", msg.From, msg, msg.Content)

		fileData := msg.Content

		// Send file to client, setting filename
		w.Header().Set("Content-Disposition", "attachment; filename="+fileName)
		w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
		w.Header().Set("Content-Length", fmt.Sprintf("%d", len(fileData)))
		w.Write(fileData)
		done = true

	}

	//p2pnet.MsgCallbacks.Store("file", frh)
	// Broadcast file request
	msgId := p2pnet.Broadcast("send file", []byte(path))
	p2pnet.MessageIdCallbacks.Store(msgId, frh)
	//Wait for file result.  If not received in 60 seconds, return error
	startTime := time.Now()
	for {
		if done {
			break
		}
		if time.Since(startTime) > 60*time.Second {
			fmt.Println("File not found", path)
			http.Error(w, "File not found", 404)
			break
		}
		time.Sleep(1 * time.Second)

	}
	p2pnet.MsgCallbacks.Delete("file result")
}

// Handle a http request to get a file from a fileserver
func fileHandler(w http.ResponseWriter, r *http.Request) {

	encoded_path := strings.TrimPrefix(r.URL.Path, "/v1/file/")
	fmt.Println("Looking up file", encoded_path)
	// Decode base64 path because the server is too dumb to handle urls properly
	path_b, _ := base64.URLEncoding.DecodeString(encoded_path)
	path := string(path_b)
	fmt.Println("Requesting file", path)

	fileName := strings.Split(path, "/")[len(strings.Split(path, "/"))-1]
	fmt.Println("Waiting for file", fileName)

	var done bool
	//File result handler
	frh := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("Got file result from %s %+v (%s)\n", msg.From, msg, msg.Content)

		fileData := msg.Content

		// Send file to client, setting filename
		w.Header().Set("Content-Disposition", "attachment; filename="+fileName)
		w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
		w.Header().Set("Content-Length", fmt.Sprintf("%d", len(fileData)))
		w.Write(fileData)
		done = true

	}

	//p2pnet.MsgCallbacks.Store("file", frh)
	// Broadcast file request
	msgId := p2pnet.Broadcast("send file", []byte(path))
	p2pnet.MessageIdCallbacks.Store(msgId, frh)
	//Wait for file result.  If not received in 60 seconds, return error
	startTime := time.Now()
	for {
		if done {
			break
		}
		if time.Since(startTime) > 60*time.Second {
			fmt.Println("File not found", path)
			http.Error(w, "File not found", 404)
			break
		}
		time.Sleep(1 * time.Second)

	}
	p2pnet.MsgCallbacks.Delete("file result")
}

// Handle a http request to open a connection to a peer at the given address
func addPeerHandler(w http.ResponseWriter, r *http.Request) {
	peer := strings.TrimPrefix(r.URL.Path, "/v1/addpeer/")
	fmt.Println("Adding peer", peer)
	p2pnet.AddEndpoint(peer)
	p2p.ExtraDebugf("Contacting known address: %v ...", peer)
	go func() {
		err := p2pnet.Connect(peer)
		if err != nil {
			p2p.ExtraDebugf("Error connecting to %v: %v\n", peer, err)
		}
	}()
}

// Handle a http request to get the shared secret
func secretHandler(w http.ResponseWriter, r *http.Request) {
	secret := strings.TrimPrefix(r.URL.Path, "/v1/secret")
	fmt.Println("Getting secret", secret)
	fmt.Fprintf(w, "%s", p2pnet.SharedPassword)
}

// Announce this node to the network
func announceHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Announcing node")
	p2pnet.Announce(p2pnet.NodeName, p2pnet.BindAddrPort)
}

// Handle a http request to notify the user
func notifyHandler(w http.ResponseWriter, r *http.Request) {
	notification_base64 := strings.TrimPrefix(r.URL.Path, "/v1/notify/")
	notification, err := base64.URLEncoding.DecodeString(notification_base64)
	if err != nil {
		fmt.Println("Error decoding notification", err)
		return
	}
	fmt.Println("Notifying user", string(notification))
	p2pnet.Broadcast("notify", []byte(notification))
}
