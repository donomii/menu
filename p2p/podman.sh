podman build -t p2p .
# Change 16172 to whatever port you want to expose from the container
podman run  --env SECRET=12345... --env PASSWORD=abcde...  --env PEER=hostname:16171 --env NAME=podman --env PORT=:16172 -p 16172:16172 p2p

