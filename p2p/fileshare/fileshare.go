package main

import (
	"flag"
	"os"
	"strings"
	"time"

	"encoding/json"
	"fmt"

	"path/filepath"

	p2p ".."
)

func getHostName() string {
	// Get the hostname
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	return hostname
}

func main() {

	// Command line options
	// -bind <ip:port>  Listen on this address
	var bindAddr string
	flag.StringVar(&bindAddr, "bind", ":16171", "Listen address")
	var peerAddr string
	flag.StringVar(&peerAddr, "peer", "", "Startup peer address")
	var SecretNetworkPhrase string
	flag.StringVar(&SecretNetworkPhrase, "secret", "", "Secret network phrase")
	var password string
	flag.StringVar(&password, "password", "", "Password")
	var nodeName string = getHostName()
	flag.StringVar(&nodeName, "name", nodeName, "Node name")
	// Directory to share
	var shareDir string
	flag.StringVar(&shareDir, "share", "", "Directory to share")
	var WantDebug bool
	flag.BoolVar(&WantDebug, "debug", false, "Debug")
	flag.BoolVar(&p2p.ExtraDebug, "extradebug", false, "Extra debug")
	flag.Parse()

	if shareDir == "" {
		fmt.Println("Please specify a directory to share with --share")
		os.Exit(1)
	}

	p, err := p2p.NewNetwork(bindAddr, []byte(SecretNetworkPhrase), []byte(password))
	if err != nil {
		panic(err)
	}
	if WantDebug {
		p2p.GlobalDebug = true
	}
	p.LoadDefaultConfig(nodeName)
	p.AddEndpoint(peerAddr)

	p.NodeName = nodeName
	p.SaveDefaultConfig(nodeName)

	//Start the network, so it is ready after the files have been scanned
	go p.Start()

	var fileList []string

	// Recursively walk the shareDir and add all files to fileList
	err = filepath.Walk(shareDir, func(path string, f os.FileInfo, err error) error {
		fileList = append(fileList, path)
		return nil
	})

	fmt.Println("Sharing the following files:")
	for _, file := range fileList {
		fmt.Println(file)
	}

	type SearchRequest struct {
		SearchString string
	}

	// Handle search requests
	se := func(network *p2p.Network, msg *p2p.Message) {
		//Unmarshal the message
		var sr = string(msg.Content)
		fmt.Printf("(%v) Saw search request %v from %v, %v hops away\n", network.BindAddrPort, sr, p2p.PublicKeyToId(p2p.DecodeRSAPublicKey(msg.From)), msg.Hops)

		var resultList []string
		//Search for the string
		for _, file := range fileList {
			//fmt.Printf("Searching for %v in %v\n", sr, file)
			if strings.Contains(file, sr) {
				//fmt.Println("Found match")
				resultList = append(resultList, file)
			}
		}

		fmt.Printf("Returning results %+v\n", resultList)
		//marshall the result
		resultBytes, _ := json.Marshal(resultList)
		//Send the result
		returnMessage := p2p.Message{
			To:             msg.From,
			ReplyToId:      msg.Id,
			Type:           "search results",
			Content:        resultBytes,
			EncodedContent: resultBytes,
		}

		fmt.Printf("(%v) Sending search results id %v, %+v to %v\n", msg.Id, network.BindAddrPort, string(returnMessage.Content), p2p.PublicKeyToId(p2p.DecodeRSAPublicKey(msg.From)))
		network.SendMessage(&returnMessage)
	}

	p.MsgCallbacks.Store("search request", se)

	//Handle send file requests
	sfe := func(network *p2p.Network, msg *p2p.Message) {
		//Unmarshal the message
		var sr = string(msg.Content)
		json.Unmarshal(msg.Content, &sr)
		fmt.Printf("(%v) Saw send file request %v from %v, %v hops away\n", network.BindAddrPort, msg.Content, msg.From, msg.Hops)

		//Check that the file is in fileList
		var fileFound bool = false
		for _, file := range fileList {
			if file == sr {
				fileFound = true
			}
		}

		if fileFound {
			//Send the file
			fmt.Printf("Sending file %v\n", sr)

			//Load the file from disk
			fileBytes, err := os.ReadFile(sr)
			if err != nil {
				fmt.Printf("Error reading file %v\n", sr)
				return
			}

			//Send the result
			returnMessage := p2p.Message{
				To:             msg.From,
				ReplyToId:      msg.Id,
				Type:           "file",
				Content:        fileBytes,
				EncodedContent: fileBytes,
			}

			fmt.Printf("(%v) Sending file %v to %v\n", network.BindAddrPort, string(sr), msg.From)
			network.SendMessage(&returnMessage)
		} else {
			fmt.Printf("File %v not found\n", sr)
		}
	}

	p.MsgCallbacks.Store("send file", sfe)

	// Announce our service
	for {

		time.Sleep(time.Duration(p.AnnounceInterval) * time.Second)
		p.SaveDefaultConfig(nodeName)
	}

}
