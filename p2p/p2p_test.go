package p2p

import (
	"crypto/rand"
	"crypto/rsa"
	
	"testing"
)

func TestCrypto(t *testing.T) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		panic(err)
	}
	publicKey := &privateKey.PublicKey
	encodedPublicKey := EncodeRSAPublicKey(publicKey)
	publicKey = DecodeRSAPublicKey(encodedPublicKey)
	testMessage := EncryptWithPublicKey([]byte("Hello world"), publicKey)
	final := DecryptWithPrivateKey(testMessage, privateKey)
	if string(final) != "Hello world" {
		t.Errorf("Decrypted message is not the same as original")
	}

}
