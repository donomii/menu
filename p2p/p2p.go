package p2p

import (
	"bufio"
	"bytes"
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
	"time"

	"encoding/base64"
	"encoding/json"

	"io"
	"log"
	"net"
	"runtime/debug"
	"strconv"
	"sync"
)

var GlobalDebug bool = false
var ExtraDebug bool = false
var GlobalVerbose bool = false

type Message struct {
	Id             string
	ReplyToId      string
	Type           string
	From           string
	To             string
	Content        []byte
	EncodedContent []byte
	Sign           []byte
	Hops           int
	Version        int
}

type Node struct {
	Conn                net.Conn `json:"-"`
	PublicKey           *rsa.PublicKey
	AESKey              [32]byte
	SymmetricKeyReceive []byte       `json:"-"`
	SymmetricKeySend    []byte       `json:"-"`
	Buff                bufio.Reader `json:"-"`
	SeenIds             *sync.Map    //Attached to nodes(and thus connections, so we can route backwards)
	LastSeen            time.Time
	SendMutex           sync.Mutex //Mutex for sending messages

}

func (n *Node) PublicKeyString() string {
	return EncodeRSAPublicKey(n.PublicKey)
}

func (n *Node) Id() string {
	return n.PublicKeyString()
}

func (n *Node) DisplayName() string {
	return KeyShortName([]byte(n.PublicKeyString()))
}

type Network struct {
	NodeName       string    // Display name of the node
	BindAddrPort   string    // Address and port to listen on
	ConnectedNodes *sync.Map // Map of connected nodes i.e. neighbours

	PrivateKey     *rsa.PrivateKey //This node's private key
	PublicKey      *rsa.PublicKey  //This node's public key
	SharedPassword []byte          //Shared password for private network.  All nodes must have the same password
	SharedKey      []byte          //Shared key for private network.  All nodes must have the same key (Pre-Shared Key)
	MsgCallbacks   *sync.Map       //Map of message types to callback functions

	Id                 string    //Id of this node, usually the public key
	Nodes              *sync.Map //All nodes that have been seen
	SeenMessages       *sync.Map //All message ids that have been seen, to prevent duplicates
	MessageCounter     int       //Number of messages sent
	DuplicateCounter   int       //Number of duplicate messages seen and dropped
	ForwardCounter     int       //Number of messages forwarded
	ReceivedCounter    int       //Number of messages received
	PossibleEndpoints  *sync.Map //All possible endpoints (ip:port) that have been seen
	AnnounceInterval   int       //How often to announce this node's existence on the network
	AutoPort           bool      //Whether to automatically increment the listening port number if the desired port is in use
	Services           *sync.Map //A collection of the currentknown services, as gathered from service announcements on the network
	MessageIdCallbacks *sync.Map //Handlers for message replies.  P2p works asynchronously, so every request has a callback id, and the reply contains that id
	Topology           *sync.Map //The current network topology, as gathered from topology announcements on the network
	Version            int       //The version of the p2p protocol
	MaxHops            int       //The maximum number of hops a message can take before being dropped
	TrustedKeys        []string  //List of trusted keys
	ConfigBaseDir      string    //The base directory for config files.  Leave unset for default(home directory/.p2p)

}

func NewNetwork(listenAddr string, sharedPassword, sharedKey []byte) (*Network, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		panic(err)
	}
	publicKey := &privateKey.PublicKey

	id := PublicKeyToId(publicKey)

	if len(sharedKey) < 32 {
		return nil, fmt.Errorf("Shared key must be at least 32 bytes")
	}
	if len(sharedPassword) < 32 {
		fmt.Printf("Shared secret should be at least 32 bytes")
	}
	if len(listenAddr) == 0 {
		panic("Listen address must be specified")
	}

	return &Network{
		BindAddrPort:       listenAddr,
		ConnectedNodes:     &sync.Map{},
		Nodes:              &sync.Map{},
		PrivateKey:         privateKey,
		PublicKey:          publicKey,
		SharedPassword:     sharedPassword,
		SharedKey:          sharedKey,
		MsgCallbacks:       &sync.Map{},
		SeenMessages:       &sync.Map{},
		PossibleEndpoints:  &sync.Map{},
		Services:           &sync.Map{},
		MessageIdCallbacks: &sync.Map{},
		Topology:           &sync.Map{},
		Id:                 id,
		AnnounceInterval:   60,
		Version:            3,
		MaxHops:            51,
	}, nil
}

func GenerateSecretPhrase() []byte {
	secret := make([]byte, 32)
	rand.Read(secret)
	return secret
}

type SearchResult struct {
	Id     string // A uuid for later use
	Name   string // The name of the file
	Node   []byte // The public key of the node that has the file
	Sample string // A sample of the file
}

type Config struct {
	Name             string
	PrivateKey       *rsa.PrivateKey
	PublicKey        *rsa.PublicKey
	KnownNodes       []string
	AnnounceInterval int
}

func (p2pnet *Network) SaveDefaultConfig(name string) {
	if name == "" {
		fmt.Println("No node name specified, will not save pki keys")
		return
	}
	config := Config{
		Name:             name,
		PrivateKey:       p2pnet.PrivateKey,
		PublicKey:        p2pnet.PublicKey,
		KnownNodes:       []string{},
		AnnounceInterval: p2pnet.AnnounceInterval,
	}
	p2pnet.PossibleEndpoints.Range(func(k, v interface{}) bool {
		config.KnownNodes = append(config.KnownNodes, k.(string))
		return true
	})

	configBase := os.Getenv("HOME") + "/.p2p/"
	if p2pnet.ConfigBaseDir != "" {
		configBase = p2pnet.ConfigBaseDir + "/"
	}

	//Filename should be <home directory>/.p2p/configs/<name>.json
	configDir := fmt.Sprintf("%snodeconfigs", configBase)
	filename := fmt.Sprintf("%s/%s.json", configDir, name)
	tempDir := fmt.Sprintf("%stmp", configBase)
	//Create the directory if it doesn't exist
	os.MkdirAll(configDir, 0700)
	os.MkdirAll(tempDir, 0700)
	//Convert the config to json
	data, _ := json.Marshal(config)
	//Set the permissions
	perm := fs.FileMode(0600)

	//Write the file.  Use save and rename to avoid partial writes
	tmpfile, err := ioutil.TempFile(tempDir, "p2p")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer os.Remove(tmpfile.Name()) // clean up
	ioutil.WriteFile(tmpfile.Name(), data, perm)
	os.Rename(tmpfile.Name(), filename)

	fmt.Println("Saved config to", filename)

}

func (p2pnet *Network) LoadDefaultConfig(name string) {
	if name == "" {
		fmt.Println("No node name specified, will not load pki keys")
		return
	}

	configBase := os.Getenv("HOME") + "/.p2p/"
	if p2pnet.ConfigBaseDir != "" {
		configBase = p2pnet.ConfigBaseDir + "/"
	}

	//Filename should be <home directory>/.p2p/configs/<name>.json
	filename := fmt.Sprintf("%snodeconfigs/%s.json", configBase, name)
	//Read the file
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	//Convert the json to a config
	var config Config
	json.Unmarshal(data, &config)
	//Set the config
	if config.Name == "" {
		fmt.Println("No node name in config file, will not load pki keys")
		return
	} else {
		p2pnet.NodeName = config.Name
	}

	if config.PrivateKey == nil {
		fmt.Println("No private key in config file, will not load pki keys")
		return
	} else {
		p2pnet.PrivateKey = config.PrivateKey
	}

	if config.PublicKey == nil {
		fmt.Println("No public key in config file, will not load pki keys")
		return
	} else {
		p2pnet.PublicKey = config.PublicKey
	}

	if config.AnnounceInterval == 0 {
		fmt.Println("No announce interval in config file, will not load")
		return
	} else {
		p2pnet.AnnounceInterval = config.AnnounceInterval
	}

	//Add the known nodes
	for _, node := range config.KnownNodes {
		Debugf("Adding known node %v from cached node list", node)
		p2pnet.AddEndpoint(node)
	}

}

func (p2pnet *Network) Start() {

	verbosef("Starting node %v on %v\n", p2pnet.NodeName, p2pnet.BindAddrPort)
	go p2pnet.Listen()

	p2pnet.LoadDefaultConfig(p2pnet.NodeName)
	p2pnet.SaveDefaultConfig(p2pnet.NodeName)

	go p2pnet.ConnectNewEndpointsLoop()

	for {

		_, bindport, _ := net.SplitHostPort(p2pnet.BindAddrPort)

		//Count peer connections
		peerConnections := 0
		p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
			peerConnections++
			return true
		})
		log.Printf("Duplicates: %v, Forwarded: %v, Sent: %v, Received: %v, Peer connections: %v\n", p2pnet.DuplicateCounter, p2pnet.ForwardCounter, p2pnet.MessageCounter, p2pnet.ReceivedCounter, peerConnections)
		log.Printf("Services: %+v\n", p2pnet.ServicesList())
		time.Sleep(time.Duration(p2pnet.AnnounceInterval) * time.Second)
		Debugf("Announcing self(%v) via broadcast to all connections\n", bindport)
		p2pnet.Announce(p2pnet.NodeName, bindport)
	}
}

func (p2pnet *Network) AddEndpoint(endpoint string) {
	ExtraDebugf("Adding possible endpoint: %v\n", endpoint)
	p2pnet.PossibleEndpoints.Store(endpoint, true)
}

func (p2pnet *Network) ConnectNewEndpointsLoop() {
	NeedWriteConfig := false
	verbosef("Starting Main Connection Loop...")
	for {

		//Get NumConnectedNodes of p2pnet.ConnectedNodes
		NumConnectedNodes := 0
		p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
			NumConnectedNodes = NumConnectedNodes + 1
			return true
		})

		p2pnet.PossibleEndpoints.Range(func(key, value interface{}) bool {
			if NumConnectedNodes < 50 { //FIXME add maxconnectednodes to config

				remoteIp := key.(string)
				//This doesn't work, we need it for testing and also for multiple service nodes on the same machine
				/*if strings.HasPrefix(remoteIp, "127.") {
					return true
				}
				if strings.HasPrefix(remoteIp, "[::1]") {
					return true
				}*/

				//Check that the ip address is a valid address
				addr, port, err := net.SplitHostPort(remoteIp)
				if err != nil {
					verbosef("Discarding %v because it is not a valid ip address\n", remoteIp)
					//Delete the endpoint
					p2pnet.PossibleEndpoints.Delete(remoteIp)
					NeedWriteConfig = true
					return true
				}
				Debugf("%v -> Host: %v, port: %v\n", remoteIp, addr, port)
				//Remove ipv6 brackets, if any
				addr = strings.Trim(addr, "[]")
				if net.ParseIP(addr) == nil {
					verbosef("Discarding %v because it is not a valid ip address\n", remoteIp)
					//Delete the endpoint
					p2pnet.PossibleEndpoints.Delete(remoteIp)
					NeedWriteConfig = true
					return true
				}

				ExtraDebugf("Contacting known address: %v ...", remoteIp)
				go func() {
					err := p2pnet.Connect(remoteIp)
					if err != nil {
						ExtraDebugf("Error connecting to %v: %v\n", remoteIp, err)
					}
				}()
			}
			if NumConnectedNodes == 0 {
				time.Sleep(100 * time.Millisecond)
			} else {
				time.Sleep(10 * time.Second)
			}
			return true
		})

		if NeedWriteConfig {
			p2pnet.SaveDefaultConfig(p2pnet.NodeName)
			NeedWriteConfig = false
		}
	}
}

func (p2pnet *Network) Listen() {
	Debugf("(%v) Listening...\n", p2pnet.BindAddrPort)
	//Parse the port number and address from p2pnet.BindAddrPort
	addr, portStr, err := net.SplitHostPort(p2pnet.BindAddrPort)
	if err != nil {
		log.Fatal("Error parsing address: ", err)
	}

	port, err := strconv.Atoi(portStr)
	if err != nil {
		log.Fatal("Error parsing port: ", err)
	}

	//Attempt to listen on p2pnet.BindAddrPort.  If the port is already in use, try the next one
	//Loop until we find a port that is not in use

	listener, err := net.Listen("tcp", p2pnet.BindAddrPort)
	if err != nil {
		for i := 1; i < 100; i++ {
			port += i
			p2pnet.BindAddrPort = addr + ":" + strconv.Itoa(port)
			listener, err = net.Listen("tcp", p2pnet.BindAddrPort)
			if err == nil {
				break
			}
		}
		if err != nil {
			log.Fatal("Error listening:", err)
		}
	}
	log.Println("Started node on ", p2pnet.BindAddrPort)
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("Error accepting connection:", err)
			continue
		}
		go func() {
			err := p2pnet.handleConnection(conn)
			if err != nil {
				log.Printf("(%v<=>%v)Error handling connection: %v\n", p2pnet.BindAddrPort, conn.RemoteAddr(), err)
			}
		}()
	}
}

func (p2pnet *Network) Connect(addr string) error {
	//Recover from panics and log them
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from panic in Connect: ", r)
			log.Println("Connection closed.")

			//Print stack trace
			buf := make([]byte, 10000)
			runtime.Stack(buf, false)
			log.Println(string(buf))

		}
	}()
	Debugf("(%v) Connecting to %v\n", p2pnet.BindAddrPort, addr)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return err
	}
	return p2pnet.handleConnection(conn)
}

func (p2pnet *Network) handleConnection(conn net.Conn) error {
	defer conn.Close()

	//Recover from panics and log them
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from panic in handleConnection: ", r)
			log.Println("Connection closed.")
			//Print stack trace
			buf := make([]byte, 10000)
			runtime.Stack(buf, false)
			log.Println(string(buf))

		}
	}()

	Debugf("(%v) Receiving connection from %v\n", p2pnet.BindAddrPort, conn.RemoteAddr())
	remoteNode, err := p2pnet.exchangeKeys(conn)
	if err != nil {
		return err
	}

	p2pnet.addNode(remoteNode)
	Debugf("(%v) Added node to collection list: %v\n", p2pnet.BindAddrPort, remoteNode.Id())
	p2pnet.Announce(p2pnet.NodeName, p2pnet.BindAddrPort)

	for {
		msg, err := p2pnet.receiveMessageDecrypt(remoteNode)
		if err != nil {
			p2pnet.removeNode(remoteNode)
			return err
		}

		//FIXMe: wasting resources decrypting messages we've already seen.  Do this sooner
		_, ok := p2pnet.SeenMessages.Load(msg.Id)
		if ok {
			//debugf("(%v) Message %v from %v already seen, ignoring\n", p2pnet.BindAddrPort, msg.Type, msg.From)

			p2pnet.DuplicateCounter++
		} else {

			go p2pnet.handleMessage(msg)
		}
		p2pnet.SeenMessages.Store(msg.Id, true)
	}
}

func Debugf(format string, args ...interface{}) {
	if GlobalDebug {
		log.Printf(format, args...)
	}
}

func verbosef(format string, args ...interface{}) {
	if GlobalVerbose {
		log.Printf(format, args...)
	}
}

func ExtraDebugf(format string, args ...interface{}) {
	if ExtraDebug {
		log.Printf("Extradebug: "+format, args...)
	}
}

func panicf(format string, args ...interface{}) {
	panic(fmt.Sprintf(format, args...))
}

func panicln(args ...interface{}) {
	panic(fmt.Sprintln(args...))
}

func (p2pnet *Network) exchangeKeys(conn net.Conn) (*Node, error) {

	//The main complication here is that we can't use the more advanced message sending routines, because they rely on the keys having been exchanged.  Instead, we have to use the raw send and receive routines, and we encrypt the messages manually.

	//The node at the other end of the connection
	remoteNode := &Node{
		Conn:    conn,
		Buff:    *bufio.NewReader(conn),
		SeenIds: &sync.Map{},
	}

	remoteAddr := conn.RemoteAddr().String()

	//Perform CHAP with shared secret

	//Send a random challenge string to the remote node
	myChallenge := make([]byte, 32)
	_, err := io.ReadFull(rand.Reader, myChallenge)
	if err != nil {
		return nil, err
	}
	myChallengeMessage := &Message{
		From:           EncodeRSAPublicKey(p2pnet.PublicKey),
		Type:           "challenge",
		EncodedContent: myChallenge,
		Version:        p2pnet.Version,
	}

	// Encrypt the challenge with the Pre-Shared Key
	encryptedChallengeMessage := p2pnet.encryptMessageToNeighbour(myChallengeMessage, p2pnet.SharedKey)
	ExtraDebugf("(%v<=>%v) Encrypted challenge message: %v\n", p2pnet.BindAddrPort, remoteAddr, encryptedChallengeMessage)
	err = p2pnet.sendBytes(remoteNode, encryptedChallengeMessage)
	if err != nil {
		return nil, err
	}

	ExtraDebugf("(%v<=>%v) Sent challenge %v\n", p2pnet.BindAddrPort, remoteAddr, myChallenge)

	//Read their challenge string
	theirEncryptedChallengeData, err := p2pnet.receiveBytes(remoteNode)
	if err != nil {
		return nil, err
	}

	/* Can accidentally reject packets
	if len(theirEncryptedChallengeData) > 0 && theirEncryptedChallengeData[0] == '{' {
		//This is a bug in older versions
		//The message was not encrypted when it should have been
		//We can't decrypt it, so we just ignore it
		fmt.Printf("[%v<=>%v] Message was not encrypted, ignoring:%v", p2pnet.BindAddrPort, remoteAddr, string(theirEncryptedChallengeData))
		return nil, fmt.Errorf("Message was not encrypted, ignoring")
	}
	*/

	ExtraDebugf("(%v<=>%v)Received encrypted challenge data: %v\n", p2pnet.BindAddrPort, remoteAddr, theirEncryptedChallengeData)
	theirChallengeMessage, err := p2pnet.decryptMessageFromNeighbour(theirEncryptedChallengeData, p2pnet.SharedKey)
	if err != nil {
		Debugf("(%v<=>%v) Error decrypting challenge message.  Remote program might be a different version or not a p2p node (e.g. portscan)`.  Error was %v\n", p2pnet.BindAddrPort, remoteAddr, err)
		return nil, err
	}

	if theirChallengeMessage.Type != "challenge" {
		return nil, fmt.Errorf("(%v) Invalid challenge type: %+v", p2pnet.BindAddrPort, theirChallengeMessage)
	}
	Debugf("Received challenge from %v\n", conn.RemoteAddr())

	if theirChallengeMessage.Version < p2pnet.Version {
		Debugf("(%v<=>%v) Their version (%v) is older than ours (%v).  Please upgrade remote node\n", p2pnet.BindAddrPort, remoteAddr, theirChallengeMessage.Version, p2pnet.Version)
		fmt.Printf("(%v<=>%v) Their version (%v) is older than ours (%v).  Please upgrade remote node\n", p2pnet.BindAddrPort, remoteAddr, theirChallengeMessage.Version, p2pnet.Version)
		Debugf("Message: %+v\n", theirChallengeMessage)
		return nil, fmt.Errorf("Their version (%v) is older than ours (%v).  Please upgrade remote node", theirChallengeMessage.Version, p2pnet.Version)
	}

	if theirChallengeMessage.Version > p2pnet.Version {
		Debugf("(%v<=>%v) Their version (%v) is newer than ours (%v).  Please upgrade this node\n", p2pnet.BindAddrPort, remoteAddr, theirChallengeMessage.Version, p2pnet.Version)
		fmt.Printf("(%v<=>%v) Their version (%v) is newer than ours (%v).  Please upgrade this node\n", p2pnet.BindAddrPort, remoteAddr, theirChallengeMessage.Version, p2pnet.Version)
		Debugf("Message: %+v\n", theirChallengeMessage)
		return nil, fmt.Errorf("Their version (%v) is newer than ours (%v).  Please upgrade this node", theirChallengeMessage.Version, p2pnet.Version)
	}

	//Calculate the correct challenge response
	theirChallenge := theirChallengeMessage.EncodedContent
	ExtraDebugf("(%v<=>%v) Received challenge %v\n", p2pnet.BindAddrPort, remoteAddr, theirChallenge)
	myResponse := sha256.Sum256(append(theirChallenge, p2pnet.SharedPassword...))
	ExtraDebugf("(%v<=>%v) Calculated response %v\n", p2pnet.BindAddrPort, remoteAddr, myResponse[:])
	p2pnet.sendRaw(remoteNode, &Message{
		From:    EncodeRSAPublicKey(p2pnet.PublicKey),
		Type:    "chapresponse",
		Content: myResponse[:],
	})

	Debugf("(%v<=>%v)Sent challenge response to %v\n", p2pnet.BindAddrPort, remoteAddr, conn.RemoteAddr())

	//Read their challenge response
	theirResponseData, err := p2pnet.receiveMessageRaw(remoteNode)
	if err != nil {
		return nil, err
	}

	Debugf("(%v<=>%v)Received challenge response from %v\n", p2pnet.BindAddrPort, remoteAddr, conn.RemoteAddr())

	if theirResponseData.Type != "chapresponse" {

		return nil, fmt.Errorf("(%v<=>%v)Invalid chap response type: %+v", p2pnet.BindAddrPort, remoteAddr, theirResponseData)
	}

	theirResponse := theirResponseData.Content
	blah := sha256.Sum256(append(myChallenge, p2pnet.SharedPassword...))

	//Check that the response is correct
	if !bytes.Equal(theirResponse, blah[:]) {
		fmt.Printf("Peer %v failed CHAP authentication\n", conn.RemoteAddr())
		fmt.Printf("Invalid chap response data: %v, expected %v\n", theirResponse, blah[:])
		return nil, fmt.Errorf("Invalid chap response data: %v, expected %v", theirResponse, blah[:])
	}

	Debugf("(%v<=>%v) CHAP successful\n", p2pnet.BindAddrPort, remoteAddr)

	// Send our public key

	//encrypt our public key with our shared secret key
	myPublicKeyString := EncodeRSAPublicKey(p2pnet.PublicKey)
	myPublicKeyEncryptedMessage := p2pnet.encryptMessageToNeighbour(&Message{
		From:           EncodeRSAPublicKey(p2pnet.PublicKey),
		Type:           "key",
		EncodedContent: []byte(myPublicKeyString),
		Content:        []byte(myPublicKeyString),
	}, p2pnet.SharedPassword)
	if err != nil {
		return nil, err
	}

	// Send our public key
	err = p2pnet.sendRaw(remoteNode, &Message{
		From:           EncodeRSAPublicKey(p2pnet.PublicKey),
		Type:           "keyWrapper",
		Content:        myPublicKeyEncryptedMessage,
		EncodedContent: myPublicKeyEncryptedMessage,
	})
	if err != nil {
		return nil, err
	}

	// Receive their public key
	theirPublicKeyData, err := p2pnet.receiveMessageRaw(remoteNode)
	if err != nil {
		return nil, err
	}
	theirPublicKeyEncryptedMessage := theirPublicKeyData.Content

	// Decrypt their public key
	theirPublicKeyMessage, err := p2pnet.decryptMessageFromNeighbour(theirPublicKeyEncryptedMessage, p2pnet.SharedPassword)
	if err != nil {
		panic(err)
	}

	if theirPublicKeyMessage.From != string(theirPublicKeyMessage.EncodedContent) {
		Debugf("(%v<=>%v) Keys don't match %+v,%v", p2pnet.BindAddrPort, remoteAddr, theirPublicKeyMessage.From, string(theirPublicKeyMessage.Content))
		return nil, fmt.Errorf("Invalid public key message: %+v", theirPublicKeyMessage)
	}

	theirPublicKeyString := theirPublicKeyMessage.From

	theirPublicKey := DecodeRSAPublicKey(theirPublicKeyString)

	// Compare their public key to ours, check if equal
	if theirPublicKeyString == myPublicKeyString {
		return nil, fmt.Errorf("(%v<=>%v)Whoops, received our own public key because we connected to ourselves", p2pnet.BindAddrPort, remoteAddr)
	}

	// Check we don't already have an open connection to them
	existingConn, ok := p2pnet.ConnectedNodes.Load(theirPublicKeyData.From)
	if ok {
		return nil, fmt.Errorf("(%v<=>%v)Already connected to %v, aborting connection", p2pnet.BindAddrPort, remoteAddr, existingConn.(*Node).Conn.RemoteAddr())
	}

	remoteNode.PublicKey = theirPublicKey

	Debugf("(%v<=>%v) Key exchange successful, connection open\n", p2pnet.BindAddrPort, remoteAddr)

	// Create and send a symmetric key
	MySymmetricKey := make([]byte, 32)
	_, err = io.ReadFull(rand.Reader, MySymmetricKey)
	if err != nil {
		return nil, err
	}

	remoteNode.SymmetricKeySend = MySymmetricKey
	encryptedSharedKey := EncryptWithPublicKey(MySymmetricKey, remoteNode.PublicKey)
	if err != nil {
		return nil, err
	}
	err = p2pnet.sendRaw(remoteNode, &Message{
		From:    EncodeRSAPublicKey(p2pnet.PublicKey),
		Type:    "sharedkey",
		Content: encryptedSharedKey,
	})
	if err != nil {
		return nil, err
	}

	//Receive their shared key, decrypt it with our private key, and store it
	encryptedSharedKeyData, err := p2pnet.receiveMessageRaw(remoteNode)
	if err != nil {
		return nil, err
	}
	theirSymmetricKey := DecryptWithPrivateKey(encryptedSharedKeyData.Content, p2pnet.PrivateKey)
	if err != nil {
		return nil, err
	}
	remoteNode.SymmetricKeyReceive = theirSymmetricKey

	Debugf("(%v<=>%v) Shared key exchange successful\n", p2pnet.BindAddrPort, remoteAddr)
	verbosef("(%v<=>%v) Connection established\n", p2pnet.BindAddrPort, remoteAddr)

	return remoteNode, nil
}

// Push a packet onto a connection, setting the from field and hops, and link-encrypting it
//
// This function does not do PKI encryption, only link-level (symmetric) encryption.  To encrypt the contents of the packet, use SendMessage()
func (p2pnet *Network) send(node *Node, msg *Message) error {
	//Recover from panics and log them
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from panic in send: ", r)
			log.Println("Connection closed.")

			//Print stack trace
			buf := make([]byte, 10000)
			runtime.Stack(buf, false)
			log.Println(string(buf))
			p2pnet.removeNode(node)

		}
	}()

	ExtraDebugf("Sending message type %v in send()\n", msg.Type)
	msg.Version = p2pnet.Version

	encryptedBytes := p2pnet.encryptMessageToNeighbour(msg, node.SymmetricKeySend)
	//fmt.Printf("Encrypted message bytes: %v\n", string(encryptedBytes))
	err := p2pnet.sendBytes(node, encryptedBytes)
	if err != nil {
		//log.Println("Error sending message:", err)
		return err
	}

	p2pnet.SeenMessages.Store(msg.Id, true) //Record id so we can check for duplicates

	return nil
}

// Send a message to a node, encrypting it with their public key, and routing it through the network
func (p2pnet *Network) SendMessage(msg *Message) string {
	//Recover from panics and log them
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from panic in SendMessage: ", r)
			log.Println("Connection closed.")

			//Print stack trace
			buf := make([]byte, 10000)
			runtime.Stack(buf, false)
			log.Println(string(buf))

		}
	}()
	ExtraDebugf("Sending message type %v in SendMessage\n", msg.Type)
	msg.Id = KeyShortName([]byte(EncodeRSAPublicKey(p2pnet.PublicKey))) + "-" + time.Now().String() + "-" + strconv.Itoa(p2pnet.MessageCounter)
	p2pnet.MessageCounter++
	msg.From = EncodeRSAPublicKey(p2pnet.PublicKey)
	msg.Hops = 0
	msg.Version = p2pnet.Version

	if msg.To == "all" {
		//msg.EncodedContent = msg.Content
	} else {
		msg.EncodedContent = EncryptWithPublicKey(msg.Content, DecodeRSAPublicKey(msg.To))
		msg.Content = nil // Don't send the content in the clear
	}

	hash := sha256.New()
	hash.Write(msg.EncodedContent)
	digest := hash.Sum(nil)

	var err error
	msg.Sign, err = rsa.SignPSS(rand.Reader, p2pnet.PrivateKey, crypto.SHA256, digest, nil)
	if err != nil {
		log.Fatalf("Error signing message: %v", err)
	}

	connectionList := []*Node{}
	p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
		connectionList = append(connectionList, value.(*Node))
		return true
	})

	Debugf("(%v) Sending message to %v connections: %v\n", p2pnet.BindAddrPort, len(connectionList), connectionList)
	// Send to all connected nodes.  Later, we can route this intelligently
	p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
		//debugf("(%v) Sending message on connection %v\n", p2pnet.BindAddrPort, value.(*Node).ID)
		//fmt.Printf("Sending message %+v in sendMessage()\n", msg)
		err := p2pnet.send(value.(*Node), msg)
		if err != nil {
			//log.Println("Error sending message:", err)
			p2pnet.ConnectedNodes.Delete(key)
		}
		return true
	})
	return msg.Id
}

// Send to a node, addressed by id, somewhere on the network, via the connection in node  FIXME use SendMessage?
func (p2pnet *Network) sendToNode(node *Node, msg *Message) error {
	ExtraDebugf("Sending message type %v in sendToNode()\n", msg.Type)
	msg.Id = KeyShortName([]byte(EncodeRSAPublicKey(p2pnet.PublicKey))) + "-" + time.Now().String() + "-" + strconv.Itoa(p2pnet.MessageCounter)

	p2pnet.SendMessage(msg)

	return nil
}

func (p2pnet *Network) sendBytes(node *Node, msgBytes []byte) error {
	node.SendMutex.Lock()
	defer node.SendMutex.Unlock()
	// Base 64 encode the message
	msgBytes = []byte(base64.StdEncoding.EncodeToString(msgBytes))

	msgBytes = append(msgBytes, '\n')

	_, err := node.Conn.Write(msgBytes)
	if err != nil {
		//log.Println("Error sending message:", err)
		return err
	}

	return nil
}

func (p2pnet *Network) sendRaw(node *Node, msg *Message) error {

	msg.Version = p2pnet.Version
	//FIXME zero out the encoded content field here
	ExtraDebugf("Sending message type %v in sendRaw()\n", msg.Type)
	msgBytes, err := json.Marshal(msg)
	if err != nil {
		log.Println("Error marshalling message:", err)
		return err
	}

	err = p2pnet.sendBytes(node, msgBytes)
	if err != nil {
		//log.Println("Error sending message:", err)
		return err
	}

	return nil
}

type Announcement struct {
	Name      string
	KnownIps  []string
	LastSeen  time.Time
	PublicKey *rsa.PublicKey
	Peers     []string
}

type ServiceAnnouncement struct {
	ServiceName     string
	ServiceAddress  string
	ServicePort     string
	ServiceProtocol string
	ServicePath     string
}

func (p2pnet *Network) Announce(myNodeName string, myBindport string) {
	//Recover from panics and log them
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from panic in Announce: ", r)
			log.Println("Connection closed.")

			//Print stack trace
			buf := make([]byte, 10000)
			runtime.Stack(buf, false)
			log.Println(string(buf))

		}
	}()
	_, port, err := net.SplitHostPort(p2pnet.BindAddrPort)
	if err != nil {
		panicf("Error splitting bind address %v: %v", p2pnet.BindAddrPort, err)
	}

	myIps := []string{}

	// Get all the local ips, ipv4 and ipv6 separately
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		panic(err)
	}

	for _, addr := range addrs {
		//Parse the ip address
		addr, _, err := net.ParseCIDR(addr.String())
		if err != nil {
			continue
		}
		// Skip loopback addresses
		if addr.IsLoopback() {
			continue
		}
		// Handle IPv6 and IPv4 addresses separately
		if addr.To4() != nil {
			myIps = append(myIps, addr.String()+":"+port)
		} else {
			myIps = append(myIps, "["+addr.String()+"]:"+port)
		}

	}

	Debugf("Announcing my ips: %+v\n", myIps)

	var peers []string
	p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
		//fmt.Printf("Announcing connection to peer: %v\n", NodeShortName(value.(*Node)))
		peers = append(peers, NodeShortName(value.(*Node)))
		return true
	})

	//fmt.Printf("Announcing to %+v\n", peers)

	p2pnet.Topology.Store(EncodeRSAPublicKey(p2pnet.PublicKey), peers)

	ann := Announcement{
		Name:      myNodeName,
		KnownIps:  myIps,
		PublicKey: p2pnet.PublicKey,
		Peers:     peers,
	}

	// Marshall the announcement into json
	annJson, err := json.Marshal(ann)
	if err != nil {
		panic(err)
	}

	ExtraDebugf("Announcement: %v\n", string(annJson))

	p2pnet.Broadcast("announce", annJson)

}

// Broadcasts bytes to all nodes.  The bytes are not examined or encrypted in any way
func (p2pnet *Network) Broadcast(ty string, payload []byte) string {
	//Recover from panics and log them
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from panic in Broadcast: ", r)

			//Print stack trace
			buf := make([]byte, 10000)
			runtime.Stack(buf, false)
			log.Println(string(buf))

		}
	}()
	ExtraDebugf("(%v) Announcing %v\n", p2pnet.BindAddrPort, string(payload))
	msg := &Message{
		To:             "all",
		Type:           ty,
		Content:        payload,
		EncodedContent: payload,
	}

	return p2pnet.SendMessage(msg)

}

func (p2pnet *Network) receiveMessageRaw(node *Node) (*Message, error) {
	msg := &Message{}
	msgBytes, err := p2pnet.receiveBytes(node)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(msgBytes, msg)
	if err != nil {
		log.Println("Error unmarshalling message:", err, " from data", string(msgBytes))
		return nil, err
	}
	msg.Hops++

	p2pnet.ReceivedCounter++

	//debugf("(%v) Received message %v from %v, %v hops\n", p2pnet.BindAddrPort, msg.Type, PublicKeyToId(DecodeRSAPublicKey(msg.From)), msg.Hops)
	//debugf("(%v) Message content: %+v\n", p2pnet.BindAddrPort, msg)
	/*
		if !p2pnet.verifyMessageSignature(msg) {
			log.Println("Error verifying message signature")
			return nil, errors.New("invalid signature")
		}

		p2pnet.decryptMessage(msg)
	*/

	remoteAddr := node.Conn.RemoteAddr().String()
	if msg.Version < p2pnet.Version {
		Debugf("(%v<=>%v) Their version (%v) is older than ours (%v).  Please upgrade remote node\n", p2pnet.BindAddrPort, remoteAddr, msg.Version, p2pnet.Version)
		fmt.Printf("(%v<=>%v) Their version (%v) is older than ours (%v).  Please upgrade remote node\n", p2pnet.BindAddrPort, remoteAddr, msg.Version, p2pnet.Version)
		Debugf("Message: %+v\n", msg)
		return nil, fmt.Errorf("Their version (%v) is older than ours (%v).  Please upgrade remote node", msg.Version, p2pnet.Version)
	}

	if msg.Version > p2pnet.Version {
		Debugf("(%v<=>%v) Their version (%v) is newer than ours (%v).  Please upgrade this node\n", p2pnet.BindAddrPort, remoteAddr, msg.Version, p2pnet.Version)
		fmt.Printf("(%v<=>%v) Their version (%v) is newer than ours (%v).  Please upgrade this node\n", p2pnet.BindAddrPort, remoteAddr, msg.Version, p2pnet.Version)
		Debugf("Message: %+v\n", msg)
		return nil, fmt.Errorf("Their version (%v) is newer than ours (%v).  Please upgrade this node", msg.Version, p2pnet.Version)
	}
	return msg, nil
}

func (p2pnet *Network) receiveBytes(node *Node) ([]byte, error) {
	msgBytes, err := node.Buff.ReadBytes('\n')
	if err != nil {
		Debugf("Error reading message from node %v in receiveMessageDecrypt: %v\n", NodeShortName(node), err)
		return nil, err
	}

	//Base64 decode the message
	msgBytes, err = base64.StdEncoding.DecodeString(string(msgBytes))
	return msgBytes, err
}

func (p2pnet *Network) receiveMessageDecrypt(node *Node) (*Message, error) {

	encryptedBytes, err := p2pnet.receiveBytes(node)
	if err != nil {
		log.Printf("Error reading message from %v in receiveMessageDecrypt:%v\n", NodeShortName(node), err)
		return nil, err
	}

	/*
		Can accidentally fail
		if encryptedBytes[0] == '{' {
			//This is a bug in older versions
			//The message was not encrypted when it should have been
			//We can't decrypt it, so we just ignore it
			fmt.Println("Message was not encrypted, ignoring")
			fmt.Printf("[%v<=>%v] Message was not encrypted, ignoring:%v", p2pnet.BindAddrPort, node.DisplayName(), string(encryptedBytes))
			return nil, fmt.Errorf("Message was not encrypted, ignoring")
		}
	*/

	msg, err := p2pnet.decryptMessageFromNeighbour(encryptedBytes, node.SymmetricKeyReceive)
	if err != nil {
		panic(err)
	}
	//fmt.Printf("Decrypted message: %+v\n", msg.Content)
	msg.Hops++

	p2pnet.ReceivedCounter++

	//record who sent the message, so we can route backwards
	node.SeenIds.Store(msg.From, true)

	if msg.Type == "ping" {
		//Message is a json list of nodes that the ping has passed through
		//We need to add ourselves to the list and then forward it
		nodeList := []string{}
		err = json.Unmarshal(msg.Content, &nodeList)
		if err != nil {
			log.Println("Error unmarshalling ping message:", err, " from data", string(msg.Content))
		}
		nodeList = append(nodeList, EncodeRSAPublicKey(p2pnet.PublicKey))
		msg.Content, err = json.Marshal(nodeList)
		if err != nil {
			log.Println("Error marshalling ping message:", err)
		}
		msg.EncodedContent = msg.Content
	}
	return msg, nil
}

func (p2pnet *Network) handleMessage(msg *Message) {
	//Recovers from panics in the callback
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from panic in callback:", r)
			debug.PrintStack()
		}
	}()

	//Forward the message to the correct node(s)
	p2pnet.routeMessage(msg)

	/* FIXME add feature toggle
	//Check the message signature to make sure it is authentic
	if !p2pnet.verifyMessageSignature(msg) {
		log.Println("Error verifying message signature")
		return
	}
	*/

	// If the message is a reply to a message that we sent
	if callback_i, ok := p2pnet.MessageIdCallbacks.Load(msg.ReplyToId); ok {
		callback := callback_i.(func(*Network, *Message))

		// If it wasn't addressed to all nodes, then decrypt it
		// TODO: is this correct?  If we hold the callback, should we always decrypt?

		verbosef("Decrypting message %v\n", msg.ReplyToId)
		msg.Content = DecryptWithPrivateKey(msg.EncodedContent, p2pnet.PrivateKey)

		verbosef("Calling callback for message %v\n", msg.ReplyToId)
		callback(p2pnet, msg)
		return
	}

	//If the message is specifically addressed to us, even if we didn't request it
	if msg.To == EncodeRSAPublicKey(p2pnet.PublicKey) {
		msg.Content = DecryptWithPrivateKey(msg.EncodedContent, p2pnet.PrivateKey)
		p2pnet.handleResponse(msg)
		return
	}

	// If the message is a broadcast message
	if msg.To == "all" {
		msg.Content = msg.EncodedContent //FIXME broadcast content is plaintext
		p2pnet.handleResponse(msg)
	}

	// If the message has passed through too many nodes
	if msg.Hops > p2pnet.MaxHops {
		log.Println("Too many hops")
		return
	}

}

// Generic function to test if v is in a slice.  Use generics you plonker!
func contains[T comparable](v T, slice []T) bool {
	for _, item := range slice {
		if item == v {
			return true
		}
	}
	return false
}

func (p2pnet *Network) verifyMessageSignature(msg *Message) bool {
	//Check the message signature to make sure it is authentic
	//If the message is not signed, then we assume it is fake
	if string(msg.Sign) == "" {
		fmt.Printf("Message from %v is not signed\n", msg.From)
		return false
	}

	//Check that the key is in our list of trusted nodes
	if !contains(msg.From, p2pnet.TrustedKeys) {
		fmt.Printf("Sender %v is not trusted\n", msg.From)
		return false
	}

	/* FIXME add feature toggle
	//Verify the message signature
	verified := VerifySignature(msg.EncodedContent, msg.Sign, DecodeRSAPublicKey(msg.From))
	if !verified {
		log.Println("Error verifying message signature")
		return false
	}
	*/

	return true
}

func VerifySignature(msg []byte, sign []byte, publicKey *rsa.PublicKey) bool {
	//Verify the message signature
	hashed := sha256.Sum256(msg)
	err := rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, hashed[:], sign)
	if err != nil {
		return false
	}
	return true
}

func (p2pnet *Network) ServicesList() []ServiceAnnouncement {
	services := []ServiceAnnouncement{}
	p2pnet.Services.Range(func(key, value interface{}) bool {
		services = append(services, value.(ServiceAnnouncement))
		return true
	})
	return services
}

// There are several default handlers for service and routing messages
func (p2pnet *Network) handleResponse(msg *Message) {
	Debugf("(%v) Received message %v from %v, %v hops\n", p2pnet.BindAddrPort, msg.Type, PublicKeyToId(DecodeRSAPublicKey(msg.From)), msg.Hops)
	switch msg.Type {
	case "service announcement":
		//Unmarshal the service announcement
		var serviceAnn ServiceAnnouncement
		err := json.Unmarshal(msg.EncodedContent, &serviceAnn)
		if err != nil {
			panic(err)
		}

		//Add the service to the list of services
		p2pnet.Services.Store(serviceAnn.ServiceName, serviceAnn)
		Debugf("(%v) Received service announcement: %v\n", p2pnet.BindAddrPort, serviceAnn)
	case "announce":
		//Unmarshal the node announcement
		var nodeAnn Announcement = Announcement{}
		err := json.Unmarshal(msg.EncodedContent, &nodeAnn)
		if err != nil {
			msg := fmt.Sprintf("Error unmarshalling node announcement: %v, %v, %+v", err, msg.Content, msg)
			panic(msg)
		}

		//Later, we want to merge instead of replace
		//Add the node to the list of nodes
		nodeAnn.LastSeen = time.Now()
		nodeAnn.PublicKey = DecodeRSAPublicKey(msg.From)
		p2pnet.Nodes.Store(msg.From, nodeAnn)
		for _, node := range nodeAnn.KnownIps {
			ExtraDebugf("Adding endpoint %v from announce message\n", node)
			p2pnet.AddEndpoint(node)
		}

		p2pnet.AddEdge(msg.From, nodeAnn.Peers)

	}

	//Even if the system dealt with it, we still let the user look at it
	callback, ok := p2pnet.MsgCallbacks.Load(msg.Type)
	if !ok {
		return
	}
	cb := callback.(func(*Network, *Message))
	cb(p2pnet, msg)
}

func (p2pnet *Network) AddEdge(nodeId string, peers []string) {
	p2pnet.Topology.Store(nodeId, peers)
	//fmt.Printf("Topology")
	//dumpTopology(p2pnet)
}

func dumpTopology(p2pnet *Network) {
	p2pnet.Topology.Range(func(key, value interface{}) bool {
		for _, peer := range value.([]string) {
			fmt.Printf("%v <=> %v\n", ShortName(key.(string)), peer)
		}
		return true
	})
}

func (p2pnet *Network) routeMessage(msg *Message) error {

	p2pnet.ForwardCounter++
	if msg.To == "all" {
		//log.Printf("(%v) Forwarding broadcast message type %v\n", p2pnet.BindAddrPort, msg.Type)
		p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
			remoteNode := value.(*Node)

			err := p2pnet.send(remoteNode, msg)
			if err != nil {
				//log.Println("Error sending message:", err)
				p2pnet.ConnectedNodes.Delete(key)
			}
			return true
		})
	} else {
		sent := 0
		//Check every node's seenIds to find which ones have seen the to node
		p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
			//log.Printf("(%v) Forwarding message type %v on one connection\n", p2pnet.BindAddrPort, msg.Type)
			remoteNode := value.(*Node)
			if _, ok := remoteNode.SeenIds.Load(msg.To); ok {
				//This connection has seen messages from the node we want to send to.  So we sent it to them

				err := p2pnet.send(remoteNode, msg)
				if err != nil {
					//log.Println("Error sending message:", err)
					p2pnet.ConnectedNodes.Delete(key)
				}
				sent = sent + 1

			}
			return true
		})
		if sent == 0 {
			verbosef("(%v) No nodes have seen %v, sending to all\n", p2pnet.BindAddrPort, KeyShortName([]byte(msg.To)))
			//We didn't find any nodes that have seen the node we want to send to
			//So we send it to everyone, and hope it gets there
			verbosef("(%v) Forwarding broadcast message type %v\n", p2pnet.BindAddrPort, msg.Type)
			p2pnet.ConnectedNodes.Range(func(key, value interface{}) bool {
				remoteNode := value.(*Node)
				err := p2pnet.send(remoteNode, msg)
				if err != nil {
					//log.Println("Error sending message:", err)
					p2pnet.ConnectedNodes.Delete(key)
				}
				sent = sent + 1
				return true
			})
		} else {
			//fmt.Printf("(%v) Sent message to %v neighbours\n", p2pnet.BindAddrPort, sent)
		}
	}

	return nil
}

func (p2pnet *Network) addNode(node *Node) {
	ExtraDebugf("(%v)Adding node %s\n", p2pnet.BindAddrPort, node.DisplayName())
	p2pnet.ConnectedNodes.Store(node.Id(), node)
}

func (p2pnet *Network) removeNode(node *Node) {
	Debugf("(%v)Removing node %s\n", p2pnet.BindAddrPort, node.DisplayName())
	p2pnet.ConnectedNodes.Delete(node.Id())
}

func NodeShortName(node *Node) string {
	if node.PublicKey == nil {
		return "public key missing, no node name available"
	}
	//Hash the public key
	hash := sha256.Sum256([]byte(EncodeRSAPublicKey(node.PublicKey)))
	//hex encode the hash
	return hex.EncodeToString(hash[:])
}

func ShortName(node string) string {
	//Hash the public key
	hash := sha256.Sum256([]byte(node))
	//hex encode the hash
	return hex.EncodeToString(hash[:])
}

func KeyShortName(key []byte) string {
	//Hash the public key
	hash := sha256.Sum256(key)
	//hex encode the hash
	return hex.EncodeToString(hash[:8])
}

func (p2pnet *Network) encryptMessageToNeighbour(msg *Message, encryptKey []byte) []byte {

	if len(encryptKey) < 32 {
		panicln("Key must be at least 32 bytes")
		return nil
	}

	//Encryption algorithm only wants 32bytes exactly
	symmetricKey := encryptKey[:32]

	if msg.To == "all" {
		msg.EncodedContent = msg.Content

	}

	//fmt.Printf("(%v) Encrypting message %+v\n", p2pnet.BindAddrPort, msg)
	if len(msg.EncodedContent) == 0 {
		panicln("EncodedContent is empty.  Content for encrypted messages Must be placed in the EncodedContent field")
		return nil
	}
	// Zero out the content to make sure we don't accidentally send it unencrypted
	//msg.Content = nil //FIXME
	//json encode message
	indata, err := json.Marshal(msg)
	if err != nil {
		panicln("Error marshalling message:", err)
		return nil
	}

	block, err := aes.NewCipher(symmetricKey)
	if err != nil {
		panicln("Error creating cipher:", err)
		return nil
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panicln("Error creating GCM:", err)
		return nil
	}

	nonce := make([]byte, gcm.NonceSize())
	rand.Read(nonce)
	encryptedData := gcm.Seal(nonce, nonce, indata, nil)
	//fmt.Printf("(%v) Encrypted message type: %v\n", p2pnet.BindAddrPort, msg.Type)
	return encryptedData
}

func (p2pnet *Network) decryptMessageFromNeighbour(data, decryptionKey []byte) (*Message, error) {

	if len(decryptionKey) < 32 {
		panicln("Shared password must be at least 32 bytes")
		return nil, fmt.Errorf("Shared password must be at least 32 bytes")
	}

	symmetricKey := decryptionKey[:32]

	block, err := aes.NewCipher(symmetricKey)
	if err != nil {
		log.Println("Error creating cipher:", err)
		return nil, fmt.Errorf("Error creating cipher: %v", err)
	}
	gcm, _ := cipher.NewGCM(block)
	nonceSize := gcm.NonceSize()
	if len(data) < nonceSize {
		panic(fmt.Errorf("decryptMessageFromNeighbour: Error decrypting message: content less than nonce size in message: %v", data))
		return nil, fmt.Errorf("decryptMessageFromNeighbour: Error decrypting message: content less than nonce size in message: %v", data)

	}
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	outdata, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		//panicln("decryptMessageFromNeighbour: Error decrypting message:", err, " from data", string(data))
		return nil, fmt.Errorf("decryptMessageFromNeighbour: Error decrypting message: %v", err)
	}

	var msg *Message = &Message{}
	err = json.Unmarshal(outdata, msg)
	if err != nil {
		panic(fmt.Sprintf("decrypt: Error unmarshalling message:%v from data %v", err, string(outdata)))
		return nil, fmt.Errorf("decrypt: Error unmarshalling message:%v from data %v", err, string(outdata))

	}

	if msg.To == "all" {
		msg.Content = msg.EncodedContent

	}

	return msg, nil
}
