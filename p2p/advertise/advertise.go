package main

import (
	"flag"
	"os"
	"time"

	"encoding/json"
	"fmt"

	p2p ".."
	"github.com/donomii/goof"
)

func getHostName() string {
	// Get the hostname
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	return hostname
}

func main() {

	mainIpAddress_s := goof.GetOutboundIP()
	mainIpAddress := mainIpAddress_s.String()
	// Command line options
	// -bind <ip:port>  Listen on this address
	var bindAddr string
	flag.StringVar(&bindAddr, "bind", ":16171", "Listen address")
	var peerAddr string
	flag.StringVar(&peerAddr, "peer", "", "Startup peer address")
	var SecretNetworkPhrase string
	flag.StringVar(&SecretNetworkPhrase, "secret", "", "Secret network phrase")
	var password string
	flag.StringVar(&password, "password", "", "Password")
	var nodeName string = getHostName()
	flag.StringVar(&nodeName, "name", nodeName, "Node name")
	var serviceAddress string
	flag.StringVar(&serviceAddress, "service-address", mainIpAddress, "Service address")
	var ServicePort string
	flag.StringVar(&ServicePort, "service-port", "80", "Service port")
	var ServiceName string
	flag.StringVar(&ServiceName, "service-name", "", "Service name")
	var ServiceProtocol string
	flag.StringVar(&ServiceProtocol, "service-protocol", "http", "Service protocol")
	var ServicePath string
	flag.StringVar(&ServicePath, "service-path", "/", "Service path")
	flag.BoolVar(&p2p.ExtraDebug, "extradebug", false, "Extra debug")
	flag.BoolVar(&p2p.GlobalDebug, "debug", false, "debug")

	flag.Parse()

	if ServicePort == "" {
		panic("Service port must be specified")
	}

	if ServiceName == "" {
		panic("Service name must be specified")
	}

	p, err := p2p.NewNetwork(bindAddr, []byte(SecretNetworkPhrase), []byte(password))
	if err != nil {
		panic(err)
	}
	p.AddEndpoint(peerAddr)

	p.NodeName = nodeName

	ha := func(network *p2p.Network, msg *p2p.Message) {

		//Pretty print msg
		//txt, _ := json.MarshalIndent(msg, "", "  ")
		//fmt.Println(string(txt))

		var ann p2p.Announcement = p2p.Announcement{}
		json.Unmarshal(msg.Content, &ann)

		fmt.Printf("(%v) Saw announce from %v, %v hops away\n", network.BindAddrPort, ann.Name, msg.Hops)

		for _, ip := range ann.KnownIps {
			network.AddEndpoint(ip)
		}
	}

	p.MsgCallbacks.Store("announce", ha)

	go p.Start()

	// Announce our service
	for {
		ann := p2p.ServiceAnnouncement{
			ServiceAddress:  serviceAddress,
			ServicePort:     ServicePort,
			ServiceName:     ServiceName,
			ServiceProtocol: ServiceProtocol,
			ServicePath:     ServicePath,
		}
		annBytes, _ := json.Marshal(ann)
		p.Broadcast("service announcement", annBytes)
		time.Sleep(time.Duration(p.AnnounceInterval) * time.Second)
	}

}
