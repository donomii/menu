= p2p

A personal p2p network for your home.

== What is it?

A personal p2p network for your home. It connects all your computers together, and provides basic services like file sharing, and network management.

== How does it work?

This is a traditional p2p network.  Each node is connected to peers, and exchanges messages to discover other nodes.

There is currently a command line client, and half a web client.

== How do I use it?

=== Install

```
    git clone https://gitlab.com/donomii/menu
    cd menu/p2p
    sh build.sh
```

=== Run

Warning:  This program generates a lot of network traffic.  It is not suitable for mobile devices, and/or any networks that charge by data volume.  The author is never responsible, under any circumstances, for any charges you incur.

This program has not been reviewed by security professionals.  It is not suitable for any use where security is important.  Do not run this on your company network.  The author is never responsible, under any circumstances, for any damage or security incidents.

```
    ./node --name "first node" --secret abcdefgh12345678
```

_Important_: The secret is the password for your network.  Every node must use exactly the same secret password.  You need to pick a long random string, and keep it secret.  If you lose it, you will need to change all your nodes.  You can use --generate-secret to generate a good random secret password.

```
    ./node --generate-secret
    Secret network phrase: b711380c37787aa4ba800fb9f78dbc824814f5f0f23e6580c2d127a181712fb3
```

Now you can start a node on another computer, and it will connect to the first node.

```
    ./node --name "second node" --secret abcdefgh12345678 --peer 192.168.0.3:9000
```

Replace 192.168.0.3 with the IP address of the first node.  You can start as many nodes as you like on the same machine, and have them connect to each other.  This is the easiest way to add services like filesharing.

```
    ./fileshare --share /tmp/files --name fileshare --peer 127.0.0.1:9000 --secret abcdefgh12345678
```

There are some other useful nodes you can run

```
    ./advertise --name advertise --secret abcdefgh12345678 --service-protocol http --service-port 8080 --service-name "My Web Server" --service-address 192.168.0.3
```

This will advertise a web server running on port 8080.  You can use the --service-protocol to advertise other protocols, like ssh, or ftp.

The advertiser broadcasts your services to your local net, so you can find them easily, even if your computer switches IP address.

```
    ./p2p service "My Web Server"
        http://192.168.0.3:8080/
```

There is also a log monitor, which broadcasts your system logs to all your other nodes.

```
    ./logmon --name logmonitor --secret abcdefgh12345678
```


== Command line client

The command line client is a simple way to interact with the network. 

```
    p2p nodes
      first node
      second node
      node 20

    p2p service "My Web Server"
      http://192.168.0.3:8080/

    p2p add 192.168.0.10:9000
```

== Web client

The web client is a work in progress.  It is available at http://localhost:8999/, when running the main node (node.exe).

== Security

I have done my best with security, however I'm not a security professional.  This is the most security without requiring the user to copy certificates between machines.  This program expects to run on a trusted network, like your home network.  Run it on the open internet at your own risk.

Even if the program is secure, I haven't taken any efforts to resist timing or trafic analysis or anything like that.  At best, people won't be able to read your messages, but they will be able to see who you are talking to.




