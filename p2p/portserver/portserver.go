package main

import (
	"flag"
	"net"
	"os"

	"time"

	"encoding/json"
	"fmt"

	p2p ".."
)

func getHostName() string {
	// Get the hostname
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	return hostname
}

var portMap map[string]net.Conn

func main() {

	// Command line options
	// -bind <ip:port>  Listen on this address
	var bindAddr string
	flag.StringVar(&bindAddr, "bind", ":16171", "Listen address")
	var peerAddr string
	flag.StringVar(&peerAddr, "peer", "", "Startup peer address")
	var SecretNetworkPhrase string
	flag.StringVar(&SecretNetworkPhrase, "secret", "", "Secret network phrase")
	var nodeName string = getHostName()
	flag.StringVar(&nodeName, "name", nodeName, "Node name")
	// Directory to share
	var sharePort string
	flag.StringVar(&sharePort, "share", "", "Port to share")
	flag.BoolVar(&p2p.ExtraDebug, "extradebug", false, "Extra debug")
	flag.BoolVar(&p2p.GlobalDebug, "debug", false, "debug")

	flag.Parse()

	if sharePort == "" {
		fmt.Println("Please specify a port to share with --share")
		os.Exit(1)
	}

	p, err := p2p.NewNetwork(bindAddr, []byte(SecretNetworkPhrase))
	if err != nil {
		panic(err)
	}

	p.LoadDefaultConfig(nodeName)
	p.AddEndpoint(peerAddr)

	p.NodeName = nodeName
	p.SaveDefaultConfig(nodeName)

	type SearchRequest struct {
		SearchString string
	}

	// Handle new connection requests
	se := func(network *p2p.Network, msg *p2p.Message) {
		//Unmarshal the message
		var sr = string(msg.Content)
		json.Unmarshal(msg.Content, &sr)
		fmt.Printf("(%v) Saw new connect request %v from %v, %v hops away\n", network.BindAddrPort, msg.Content, msg.From, msg.Hops)

		// Open a tcp connection to shareport
		conn, err := net.Dial("tcp", ":"+sharePort)
		if err != nil {
			fmt.Printf("Error opening connection to %v: %v\n", sharePort, err)
			return
		}

		portMap[msg.From] = conn

		returnMessage := p2p.Message{
			To:             msg.From,
			ReplyToId:      msg.Id,
			Type:           "connection open",
			Content:        nil,
			EncodedContent: nil,
		}

		fmt.Printf("(%v) Sending connection open %+v to %v\n", network.BindAddrPort, string(returnMessage.Content), p2p.KeyShortName([]byte(msg.From)))
		network.SendMessage(&returnMessage)
		//Start a watcher thread to read data from the connection, and send it as messages to the other node
		go func() {
			for {
				//Read data from the connection
				data := make([]byte, 1024)
				n, err := conn.Read(data)
				if err != nil {
					fmt.Printf("Error reading from connection to %v: %v\n", sharePort, err)
					return
				}

				//Send the data as a message
				returnMessage := p2p.Message{
					To:             msg.From,
					Type:           "data",
					Content:        data[:n],
					EncodedContent: data[:n],
				}
				fmt.Printf("(%v) Sending connection data to %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
				network.SendMessage(&returnMessage)
			}
		}()

	}

	p.MsgCallbacks.Store("open connection", se)

	//Handle data messages
	sfe := func(network *p2p.Network, msg *p2p.Message) {
		//Unmarshal the message
		var sr = string(msg.Content)
		json.Unmarshal(msg.Content, &sr)
		fmt.Printf("(%v) Saw data %v from %v, %v hops away\n", network.BindAddrPort, msg.Content, msg.From, msg.Hops)

		//Lookup the from address in the port map
		conn, found := portMap[msg.From]
		if !found {
			fmt.Printf("Connection not found for %v\n", msg.From)
			return
		}

		//Write the data to the connection
		_, err := conn.Write(msg.Content)
		if err != nil {
			fmt.Printf("Error writing to connection to %v: %v\n", sharePort, err)
			return
		}

	}

	p.MsgCallbacks.Store("data", sfe)

	go p.Start()

	// Announce our service
	for {

		time.Sleep(time.Duration(p.AnnounceInterval) * time.Second)
		p.SaveDefaultConfig(nodeName)
	}

}
