package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"encoding/json"

	p2p ".."
	"github.com/donomii/goof"
	"gitlab.com/donomii/menu/frogpond"
)

var p2pnet *p2p.Network
var frogPondNode *frogpond.Node

func getHostName() string {
	// Get the hostname
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	return hostname
}

func main() {

	// Command line options
	// -bind <ip:port>  Listen on this address
	var bindAddr string
	flag.StringVar(&bindAddr, "bind", ":16171", "Listen address")
	var peerAddr string
	flag.StringVar(&peerAddr, "peer", "", "Startup peer address")
	var SecretNetworkPhrase string
	flag.StringVar(&SecretNetworkPhrase, "secret", "", "Secret network phrase")
	var nodeName string = getHostName()
	flag.StringVar(&nodeName, "name", nodeName, "Node name")
	var password string
	flag.StringVar(&password, "password", "", "Password")
	var generateSecret bool
	flag.BoolVar(&generateSecret, "generate-secret", false, "Generate secret network phrase")
	var WantDebug bool
	flag.BoolVar(&WantDebug, "debug", false, "Debug")
	flag.BoolVar(&p2p.ExtraDebug, "extradebug", false, "Extra debug")
	flag.Parse()

	if generateSecret {
		//Generate a secret network phrase and print it as hex
		phrase := p2p.GenerateSecretPhrase()
		fmt.Printf("Secret network phrase: %x\n", phrase)
		os.Exit(0)
	}

	frogPondNode = frogpond.NewNode()

	p := SetupNode(bindAddr, peerAddr, SecretNetworkPhrase, nodeName, password, frogPondNode, WantDebug)
	p2pnet = p

	go p.Start()

	for {
		//Ping regularly
		p.Broadcast("ping", []byte("[]"))
		time.Sleep(120 * time.Second)
		p.SaveDefaultConfig(nodeName)
	}

}

func SetupNode(bindAddr, peerAddr, SecretNetworkPhrase, nodeName, password string, fp *frogpond.Node, WantDebug bool) *p2p.Network {

	p, err := p2p.NewNetwork(bindAddr, []byte(SecretNetworkPhrase), []byte(password))
	if err != nil {
		panic(err)
	}
	if WantDebug {
		p2p.GlobalDebug = true
	}
	p.LoadDefaultConfig(nodeName)

	p.AddEndpoint(peerAddr)

	p.NodeName = nodeName

	p.SaveDefaultConfig(nodeName)

	//notify handler
	not := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("NOTIFY: %v\n", string(msg.Content))
		goof.QC([]string{"./universal_menu_floaty.exe", "--size", "256", "-text", string(msg.Content), "--timeout", "2"})
	}

	p.MsgCallbacks.Store("notify", not)

	//Ping handler
	pinghandler := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("(%v) Ping from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
		//fmt.Printf("(%v) Path: %v %v\n", network.BindAddrPort, string(msg.Content), string(msg.EncodedContent)	)
		// unpack json list from content and print it
		var path []string
		json.Unmarshal(msg.Content, &path)
		p2p.Debugf("trace ping path")
		for _, hop := range path {
			p2p.Debugf("(%v) %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(hop)))
		}
		//Reply with pong
		replyMessage := p2p.Message{
			Type:           "pong",
			To:             msg.From,
			Content:        msg.Content,
			EncodedContent: msg.Content,
		}
		network.SendMessage(&replyMessage)
	}

	p.MsgCallbacks.Store("ping", pinghandler)

	//Pong handler
	ponghandler := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("(%v) Pong from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
		//fmt.Printf("(%v) Path: %v %v\n", network.BindAddrPort, string(msg.Content), string(msg.EncodedContent)	)
		var path []string
		json.Unmarshal(msg.Content, &path)
		for i, hop := range path {
			fmt.Printf("(%v) Pong hop: %v, %v\n", network.BindAddrPort, i, p2p.KeyShortName([]byte(hop)))
		}
	}

	//Frogpond datapoint update handler
	datapointhandler := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("(%v) Datapoint from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
		//fmt.Printf("(%v) Path: %v %v\n", network.BindAddrPort, string(msg.Content), string(msg.EncodedContent)	)
		var dp []frogpond.DataPoint
		json.Unmarshal(msg.Content, &dp)
		fp.AppendDataPoints(dp)
	}

	p.MsgCallbacks.Store("frogpond datapoint", datapointhandler)
	p.MsgCallbacks.Store("pong", ponghandler)

	return p

}
