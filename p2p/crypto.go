package p2p

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"

	"encoding/json"
)

func EncryptWithPublicKey(plaintext []byte, publicKey *rsa.PublicKey) []byte {
	l := publicKey.Size()
	//Break the plaintext into chunks and encrypt each chunk
	//This is necessary because the RSA algorithm can only encrypt messages
	//that are smaller than the key size
	var ciphertext [][]byte
	for i := 0; i < len(plaintext); i += l / 2 { //FIXME: size
		end := i + l/2
		if end > len(plaintext) {
			end = len(plaintext)
		}
		chunk := plaintext[i:end]
		ExtraDebugf("Encrypting chunk: %s\n", chunk)
		hash := sha256.New()
		ciphertextChunk, err := rsa.EncryptOAEP(hash, rand.Reader, publicKey, chunk, nil)
		if err != nil {
			panicf("Error encrypting: %s", err)
		}
		ciphertext = append(ciphertext, ciphertextChunk)
	}
	//Marshall the ciphertext into JSON
	ciphertextJson, err := json.Marshal(ciphertext)
	if err != nil {
		panicf("Error marshalling ciphertext: %s", err)
	}
	return ciphertextJson
}

func DecryptWithPrivateKey(ciphertext []byte, privateKey *rsa.PrivateKey) []byte {
	//Unmarshall the ciphertext from JSON
	var ciphertextJson [][]byte
	err := json.Unmarshal(ciphertext, &ciphertextJson)
	if err != nil {
		panicf("Error unmarshalling ciphertext: %s", err)
	}
	//Decrypt each chunk
	var plaintext []byte
	for _, ciphertextChunk := range ciphertextJson {
		//Debugf("Decrypting chunk: %s\n", ciphertextChunk)
		hash := sha256.New()
		plaintextChunk, err := rsa.DecryptOAEP(hash, rand.Reader, privateKey, ciphertextChunk, nil)
		if err != nil {
			panicf("Error decrypting: %s", err)
		}
		plaintext = append(plaintext, plaintextChunk...)
	}
	ExtraDebugf("Decrypted: %s", plaintext)
	return plaintext
}

func EncodeRSAPublicKey(pub *rsa.PublicKey) string {
	pubASN1, _ := x509.MarshalPKIXPublicKey(pub)

	pubBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: pubASN1,
	})

	encodedKey := base64.StdEncoding.EncodeToString(pubBytes)
	return encodedKey
}

func DecodeRSAPublicKey(encodedKey string) *rsa.PublicKey {
	pubBytes, err := base64.StdEncoding.DecodeString(encodedKey)
	if err != nil {
		panicf("failed to decode base64 encoded public key: %v", err)
	}

	block, _ := pem.Decode(pubBytes)
	if block == nil || block.Type != "RSA PUBLIC KEY" {
		panic("failed to decode PEM block containing RSA public key")
	}

	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		panicf("failed to parse DER encoded public key: %v", err)
	}

	rsaPubKey, ok := pub.(*rsa.PublicKey)
	if !ok {
		panic("failed to cast parsed public key to RSA public key")
	}

	return rsaPubKey
}

func PublicKeyToId(publicKey *rsa.PublicKey) string {
	//Hash the public key to get the ID
	hash := sha256.New()
	hash.Write(x509.MarshalPKCS1PublicKey(publicKey))
	id := base64.StdEncoding.EncodeToString(hash.Sum(nil))
	return id
}
