# Floaty

A floating icon for your desktop

# Features

Floaty creates a small icon that floats on your screen.  You can choose the icon, and what happens when you click it.

There are some bundled icons, or you can create your own.

# Build

    go build -a .

# Use

    ./floaty osascript -e "tell app \"Terminal\" to do script \"cd ~/git/menu/floaty && go build .\""

Floaty will display a default icon and then run the osascript command.  You can give any command after ./floaty, it will be run in a shell.

	./floaty shutdown

shuts down the computer

./floaty /Applications/Photos

opens photos, etc.

If you want to see the command running, you need to arrange for a new shell, which is what ```./floaty osascript -e "tell app \"Terminal\" ``` does






Floaty has many, many options.  All options go before the click command.  Run `floaty --help` to get the current list.

e.g. to make the background transparent, add the --transparent option

    ./floaty --transparent osascript -e "tell app \"Terminal\" to do script \"cd ~/git/menu floaty && go build .\""



```
  -capture-output
    	Capture output of command
  -font-size float
    	Font size (default 16)
  -internal
    	Load icon from internal store
  -list-internal
    	List bundled icons
  -no-auto-retina
    	Switch off automatic retina scaling
 -retina
    	Scale correctly for retina displays.  Requires -no-auto-retina
 -picture string
    	PNG, JPG or GIF to display on tile
  -svg string
    	SVG to display on tile
  -text string
    	Initial text (default "Floaty")
  -transparent
    	Respect alpha values in picture

```

## capture-output

Captures the output of your click command, and displays it in the floaty icon.

## font-size

Font size for all text inside the icon

## picture

Path to a PNG, JPG or GIF file to use as the icon.  Must be 128x128 pixels

## svg

Load an SVG file and use it as an icon

## internal

Floaty contains the [Remix](https://remixicon.com/) icon set.  Use --list-internal to see the list of available icons, then use it with --svg and --internal to display the icon.  

    ./floaty --internal --svg internal_icons/Business/inbox-unarchive-fill.svg

## text

Display text on the icon

## transparent

Respect the transparency in the image.  Without this, the icon will be an entirely opaque square

## no-auto-retina

Do not attempt to automatically detect retina mode.

Floaty will try to detect if it is on a retina screen, and switch to the correct picture size.  If your icon is too small or too large(and you can only see one corner), you might be in the wrong mode.  Use this switch to turn off autodetect, then use --retina to force retina mode on or off.

## retina=0 / 1

A boolean (0=false, 1=true) that forces retina mode on or off.  Requires no-auto-retina.


