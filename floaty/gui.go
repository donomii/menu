// gui.go
package main

import (
	"fmt"
	"image"
	"io"
	"log"
	"math/rand"
	"os"
	"runtime/debug"

	"github.com/srwiley/oksvg"
	"github.com/srwiley/rasterx"

	"github.com/donomii/glim"
)

var foreColour, backColour *glim.RGBA

func renderEd(targetWidth, targetHeight int) {
	defer func() {
		if r := recover(); r != nil {
			debug.PrintStack()
			fmt.Println("Recovered in renderEd", r)

		}
	}()
	left := 0
	top := 0

	if ed != nil {
		log.Println("Starting editor draw")

		size := targetWidth * targetHeight * 4
		log.Println("Clearing", size, "bytes(", targetWidth, "x", targetHeight, ")")
		backColour = &glim.RGBA{0, 0, 0, 0}
		if picture_path != "" {
			for i := 0; i < size; i = i + 1 {
				transfer_texture[i] = 0
			}
			background_picture, background_pictureX, background_pictureY := glim.LoadImage(picture_path)
			for y := 0; y < background_pictureY; y++ {
				for x := 0; x < background_pictureX; x++ {
					for rgba := 0; rgba < 4; rgba++ {
						bp := y*background_pictureX*4 + x*4 + rgba
						i := y*edWidth*4 + x*4 + rgba
						//log.Printf("x,y: %v,%v.  bpX,bpY: %v,%v. edWidth,edHeight: %v,%v.  i, bpi: %v,%v\n", x, y, background_pictureX, background_pictureY, edWidth, edHeight, i, bp)
						transfer_texture[i] = background_picture[bp]
					}
				}
			}
		} else {
			if svg_path != "" {

				background_pictureY, background_pictureX := targetWidth, targetHeight
				var err error
				var in io.ReadCloser
				if conf.Internal {
					in, err = embeddedFS.Open(svg_path)
					if err != nil {
						panic(err)
					}
					defer in.Close()
				} else {
					in, err = os.Open(svg_path)
					if err != nil {
						panic(err)
					}
					defer in.Close()
				}
				icon, err := oksvg.ReadIconStream(in)
				if err != nil {
					panic(err)
				}
				icon.SetTarget(0, 0, float64(targetWidth), float64(targetHeight))
				rgba_image := image.NewRGBA(image.Rect(0, 0, targetWidth, targetHeight))

				ras := rasterx.NewDasher(targetWidth, targetHeight, rasterx.NewScannerGV(targetWidth, targetHeight, rgba_image, rgba_image.Bounds()))
				icon.Draw(ras, 1)

				fmt.Println(" the image\n")
				fmt.Printf("Source size: %v,%v target size: %v,%v\n", background_pictureX, background_pictureY, targetWidth, targetHeight)
				pic, _, _ := glim.GFormatToImage(rgba_image, nil, targetWidth, targetHeight)
				for y := 0; y < background_pictureY; y++ {
					for x := 0; x < background_pictureX; x++ {
						backgroundpixel_index := y*background_pictureX*4 + x*4

						if pic[backgroundpixel_index+0]+pic[backgroundpixel_index+1]+pic[backgroundpixel_index+2]+pic[backgroundpixel_index+3] > 1 {
							fmt.Print("*")
						} else {
							fmt.Print("_")
						}
						for rgba := 0; rgba < 4; rgba++ {

							bp := backgroundpixel_index + rgba
							i := y*targetWidth*4 + x*4 + rgba

							if rgba != 3 {
								transfer_texture[i] = pic[backgroundpixel_index+3]
								continue
							}
							//log.Printf("x,y: %v,%v.  bpX,bpY: %v,%v. edWidth,edHeight: %v,%v.  i, bpi: %v,%v\n", x, y, background_pictureX, background_pictureY, edWidth, edHeight, i, bp)
							transfer_texture[i] = pic[bp]
						}
					}
					fmt.Println()
				}
				fmt.Println("done the image\n")
				//fmt.Printf("image: %v", pic)

			} else {

				patternColour := &glim.RGBA{128, 100, 100, 255}
				foreColour = &glim.RGBA{255, 255, 255, 255}
				num := rand.Intn(100)
				log.Println("Background rand:", num)
				for i := 0; i < size; i = i + 4 {
					var Colour *glim.RGBA
					if (i^int(i/targetWidth))%3 == 0 {
						//if !(int(math.Pow(float64(i), float64(int(i/w))))%9>0){
						Colour = patternColour
					} else {
						Colour = backColour
					}
					transfer_texture[i] = ((*Colour)[0])
					transfer_texture[i+1] = ((*Colour)[1])
					transfer_texture[i+2] = ((*Colour)[2])
					transfer_texture[i+3] = ((*Colour)[3])
				}

			}

		}

		form = ed.ActiveBuffer.Formatter
		form.Colour = foreColour
		form.Outline = true

		mouseX := 10
		mouseY := 10
		displayText := ed.ActiveBuffer.Data.Text

		log.Println("Render paragraph", string(displayText))

		ed.ActiveBuffer.Formatter.FontSize = fontSize
		glim.RenderPara(ed.ActiveBuffer.Formatter,
			0, 0, 0, 0,
			targetWidth, targetHeight, targetWidth, targetHeight,
			int(mouseX)-left, int(mouseY)-top, transfer_texture, displayText,
			false, true, false)
		log.Println("Finished render paragraph")
	}
}
