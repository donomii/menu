# Frogpond

Frogpond is a simple, INSECURE peer-to-peer key value store and host discovery service.  I use it on all my home computers to find each other and share configuration and a small amount of data.

Frogpond is a library that can be used in other applications, and there is also a stand-alone server and client that can connect to the embedded library or work entirely on their own.

There is a simple demonstration of the library used in the "trayheadless" program, which keeps my Raspberry Pis and other small computers in contact with my desktop.

## Insecure

Frogpond is not secure.  It is not encrypted, and it does not authenticate.  It is not intended for use on the Internet.  It is intended for use on a local network, where the only people who can see the traffic are people you trust.

Do not, under any circumstances, use Frogpond on the Internet.

## Installation

    export GO111MODULE=auto
    go get github.com/donomii/racketprogs/frogpond 
    go install github.com/donomii/racketprogs/frogpond 

## Use

Frogpond can be used as a library, or as a stand-alone server and client.

For standalone, run the server one one machine with

    ./server --name "Server 1"

and on another

    ./server --peer xxx.xxx.xxx.xxx --name "Server 2"

where xxx.xxx.xxx.xxx is the IP address of the first server.  You can also use the hostname of the first server.

Then run the client on either machine

    ./client add testkey testvalue
    ./client get testkey


move to the second machine and run

    ./client get testkey

and you should see the value.

If you don't see your value on the second machine, try running

    ./client ips

to list the known peers.


The complete list of commands for the client:

    ips               - Print the ips of known frogpond servers
    dump              - Dump all data from the frogpond data pool
    add [key] [value] - Add a key/svalue pair to the frogpond data pool
    delete [key]      - Delete a key/value from the frogpond data pool
    get [key]         - Get a value from the frogpond data pool

## Library Use

The best example is the frogpond stand alone server.  Mainly you have to run StartServer() and then UpdatePeers() and UpdatePeersData() periodically.  

## Security

Frogpond communicates over HTTP, and does not authenticate or encrypt.  It is not secure.  It is intended for use on a local network, where the only people who can see the traffic are people you trust.

Do not use Frogpond on the Internet.

## Design

Frogpond communicates with other Frogpond servers using HTTP.  It uses a simple REST API to add, delete, and get key/value pairs.  It also uses a simple REST API to get the IP addresses of other Frogpond servers.

The APIs for P2P and data transfer are separate.  This allows Frogpond to be used as a simple host discovery service, and/or as a simple key/value store.

All values are kept in memory, there is no persistence.

Every time you modify the data pool, Frogpond updates its peers.  This means that if you add a key/value pair, it will be available to all peers within a few seconds.

Each Frogpond server regularly checks in with its peers, by default every 50s.  This allows updates to move through NATs and firewalls, but with a larger delay than usual.


