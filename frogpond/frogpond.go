package frogpond

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"
)

type DataPoint struct {
	Key     []byte
	Value   []byte
	Name    string
	Updated time.Time
	Deleted bool
}

type DataPoolList []DataPoint

func (a DataPoolList) Len() int           { return len(a) }
func (a DataPoolList) Less(i, j int) bool { return bytes.Compare(a[i].Key, a[j].Key) < 0 }
func (a DataPoolList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

type DataPoolMap map[string]DataPoint

type Node struct {
	DataPoolLock sync.Mutex
	DataPool     DataPoolMap
}

// Create a new frogpond node
func NewNode() *Node {
	return &Node{DataPool: DataPoolMap{}}
}

// Convert a map of data points to a list of data points
func DataMap2DataList(dm DataPoolMap) DataPoolList {
	out := DataPoolList{}
	for _, v := range dm {
		out = append(out, v)
	}
	sort.Sort(out)
	return out
}

// Convert a list of data points to a map of data points
func DataList2DataMap(dl DataPoolList) DataPoolMap {
	out := DataPoolMap{}
	for _, v := range dl {
		out[string(v.Key)] = v
	}
	return out
}

// The data comes in as a list, so apply each update in turn
func (n *Node) applyUpdate(dl DataPoolList) DataPoolList {
	n.DataPoolLock.Lock()
	defer n.DataPoolLock.Unlock()

	delta := DataPoolList{}

	for _, v := range dl {

		if _, ok := n.DataPool[string(v.Key)]; !ok {
			n.DataPool[string(v.Key)] = v
			delta = append(delta, v)
			fmt.Printf("%v does not exist, adding\n", string(v.Key))
		} else {

			patchTime := v.Updated.Unix()
			datumTime := n.DataPool[string(v.Key)].Updated.Unix()
			timeDiff := patchTime - datumTime

			if timeDiff > 0 {
				n.DataPool[string(v.Key)] = v
				delta = append(delta, v)

				fmt.Printf("%v changed (candidate copy %v is newer than current copy  %v) diff: %v\n", string(v.Key), patchTime, datumTime, timeDiff)
			} else {
				fmt.Printf("%v NOT changed (candidate copy %v is older than current copy  %v) diff: %v\n", string(v.Key), patchTime, datumTime, timeDiff)
			}
		}
	}

	return delta
}

// Dump the entire data pool as a json array
func (n *Node) JsonDump() []byte {
	out, err := json.Marshal(DataMap2DataList(n.DataPool))
	if err != nil {
		log.Println("Failed to marshal data pool", err)
		return nil
	}
	return out
}

// Set a single data point
func (n *Node) SetDataPoint(key string, val []byte) []DataPoint {

	return n.AppendDataPoint(DataPoint{Key: []byte(key), Value: val, Updated: time.Now()})
}

// Set a single data point with a prefix, e.g. "/foo/" and "bar" becomes "/foo/bar"
func (n *Node) SetDataPointWithPrefix(prefix, key string, val []byte) []DataPoint {
	keyStr := fmt.Sprintf("%v%v", prefix, key)
	return n.SetDataPoint(keyStr, val)
}

// Set a single data point with a prefix, e.g. "/foo/" and "bar" becomes "/foo/bar"
func (n *Node) SetDataPointWithPrefix_str(prefix, key string, val string) []DataPoint {
	keyStr := fmt.Sprintf("%v%v", prefix, key)
	return n.SetDataPoint(keyStr, []byte(val))
}

// Set a single data point with a prefix, e.g. "/foo/" and "bar" becomes "/foo/bar"
func (n *Node) SetDataPointWithPrefix_iface(prefix, key string, val interface{}) []DataPoint {
	keyStr := fmt.Sprintf("%v%v", prefix, key)
	valStr := fmt.Sprintf("%v", val)
	return n.SetDataPoint(keyStr, []byte(valStr))
}

// Get a single data point
func (n *Node) GetDataPoint(keyStr string) DataPoint {
	n.DataPoolLock.Lock()
	defer n.DataPoolLock.Unlock()
	return n.DataPool[keyStr]
}

// Get a single data point with a prefix, e.g. "/foo/" and "bar" becomes "/foo/bar"
func (n *Node) GetDataPointWithPrefix(prefix, key string) DataPoint {
	keyStr := fmt.Sprintf("%v%v", prefix, key)
	return n.GetDataPoint(keyStr)
}

func (n *Node) DeleteDataPoint(keyStr string) []DataPoint {
	n.DataPoolLock.Lock()
	defer n.DataPoolLock.Unlock()
	n.DataPool[keyStr] = DataPoint{Key: []byte(keyStr), Deleted: true, Updated: time.Now()}
	return []DataPoint{n.DataPool[keyStr]}
}

func (n *Node) DeleteDataPointWithPrefix(prefix, key string) []DataPoint {
	n.DataPoolLock.Lock()
	defer n.DataPoolLock.Unlock()
	keyStr := fmt.Sprintf("%v%v", prefix, key)
	n.DataPool[keyStr] = DataPoint{Key: []byte(keyStr), Deleted: true, Updated: time.Now()}
	return []DataPoint{n.DataPool[keyStr]}
}

func (n *Node) DeleteAllMatchingPrefix(keyStr string) []DataPoint {
	n.DataPoolLock.Lock()
	defer n.DataPoolLock.Unlock()
	out := []DataPoint{}
	for k, v := range n.DataPool {
		if strings.HasPrefix(k, keyStr) {
			v.Deleted = true
			v.Updated = time.Now()
			n.DataPool[k] = v
			out = append(out, v)
		}
	}
	return out
}

func (n *Node) GetAllMatchingPrefix(keyStr string) []DataPoint {
	n.DataPoolLock.Lock()
	defer n.DataPoolLock.Unlock()
	out := []DataPoint{}
	for k, v := range n.DataPool {
		if strings.HasPrefix(k, keyStr) {
			out = append(out, v)
		}
	}
	return out
}


// Add or update a list of data points to the data pool
func (n *Node) AppendDataPoints(dataPoints []DataPoint) []DataPoint {

	return n.applyUpdate(dataPoints)

}

// Add or update a single data point to the data pool
func (n *Node) AppendDataPoint(dataPoint DataPoint) []DataPoint {
	
	return n.applyUpdate(DataPoolList{dataPoint})

}

type Config struct {
	HttpPort           uint
	StartPagePort      uint
	Name               string
	MaxUploadSize      uint
	Networks           []string
	KnownPeers         []string
	ArpCheckInterval   int
	PeerUpdateInterval int
}
