package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	frogpond "../frogpond"
)

func startLocalServer(startpageport uint) {

	server2 := http.NewServeMux()

	server2.HandleFunc("/localnetwork", renderLocalNetwork)
	server2.HandleFunc("/internals", renderInternals)
	server2.HandleFunc("/", renderLandingPage)

	log.Println("Server started on: 0.0.0.0:", startpageport)
	http.ListenAndServe(fmt.Sprintf("127.0.0.1:%v", startpageport), server2)
}

func renderLandingPage(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte(landingPage()))
}

func landingPage() string {
	return `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
	<title>UMH</title>
	  </head>
	    <body>
		<h1>UMH</h1>
		<p>Local network display</p>
		<p><a href="/localnetwork">Local Network</a></p>
		<p><a href="/internals">Internals</a></p>
		</body>
		</html>`
}

func renderLocalNetwork(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte(networkTemplate()))
}

func networkTemplate() string {
	//Build hosts display
	var displayHosts []string
	for _, host := range frogpond.AllHosts() {
		if host.LastSeen.Add(30 * time.Minute).After(time.Now()) {
			displayHosts = append(displayHosts, fmt.Sprintf("<li>%v(%v), last seen %v seconds ago</li>", host.Name, host.Ip, time.Now().Sub(host.LastSeen).Seconds()))

			//Add basic p2p entry

			displayHosts = append(displayHosts,
				fmt.Sprintf(
					"<li>Service: <a href='%v'>%v</a></li>",
					fmt.Sprintf("%v://%v%v",
						"frogpond",
						frogpond.FormatHttpIpPort_i(host.Ip, host.Port),
						""),
					host.Name+" - "+"p2p"+" - "))

			//Loop over services
			for _, service := range host.Services {
				displayHosts = append(displayHosts,
					fmt.Sprintf(
						"<li>Service: <a href='%v'>%v</a></li>",
						fmt.Sprintf("%v://%v%v",
							service.Protocol,
							frogpond.FormatHttpIpPort_i(host.Ip, uint(service.Port)),
							service.Path),
						host.Name+" - "+service.Name+" - "+service.Description))
			}

			displayHosts = append(displayHosts, fmt.Sprintf("<li><a href='%v'>%v</a></li>", fmt.Sprintf("%v://%v/", "http", frogpond.FormatHttpIpPort_i(host.Ip, 80)), host.Name))
		}
	}
	return `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>UMH</title>
  </head>
  <body>
<h1>Local Network Display</h1>` + strings.Join(displayHosts, "") + `
 </body>
</html>`

}

func renderInternals(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte(Internals()))
}

func Internals() string {

	return `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Internal config</title>
  </head>
  <body>
<h1>Config</h1>` + fmt.Sprintf("%+v", Configuration) + `
<br/><h1>Hosts</h1>` + fmt.Sprintf("%+v", frogpond.AllHosts()) + `
<h1>Info</h1>` + fmt.Sprintf("%+v", Info) + `
<br/><h1>Ports To Scan</h1>` + fmt.Sprintf("%+v", frogpond.PortsToScan) + `

 </body>
</html>`

}
