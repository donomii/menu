package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"

	"../tray/icon"

	menu ".."

	p2p "../p2p"
	"github.com/donomii/goof"
	"github.com/lucor/systray"
)

var Version = 1
var noScan bool

var setStatus func(string)

var (
	netmenu2 *systray.MenuItem
)

var p2pnet *p2p.Network

type ConfigStruct struct {
	HttpPort           uint
	StartPagePort      uint
	Name               string
	MaxUploadSize      uint
	Networks           []string
	KnownPeers         []string
	ArpCheckInterval   int
	PeerUpdateInterval int
	PreSharedKey       string
	NetworkPassword    string
	PeerPort           uint
}

type Service struct {
	Name        string
	Ip          string
	Port        int
	Protocol    string
	Description string
	Global      bool
	Path        string
}

type InfoStruct struct {
	GUID     string
	Name     string
	Services []Service
}

var Info InfoStruct
var Configuration ConfigStruct

func LoadConfig() ConfigStruct {
	data, err := ioutil.ReadFile("config/p2p.json")
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &Configuration)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Loaded config from %v: %+v\n", "config/p2p.json", Configuration)
	return Configuration
}

func LoadInfo() {

	fmt.Println("Loading info")
	data, err := ioutil.ReadFile("config/public_info.json")
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &Info)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Loaded info: %+v\n", Info)
}

func UberMenu() *menu.Node {
	node := menu.MakeNodeLong("Main menu",
		[]*menu.Node{
			// menu.AppsMenu(),
			// menu.HistoryMenu(),
			// menu.GitMenu(),
			// gitHistoryMenu(),
			// fileManagerMenu(),
			// menu.ControlMenu(),
		},
		"", "")
	return node
}

func main() {
	os.Chdir(goof.ExecutablePath())
	fmt.Println("Starting UMH version", Version)
	var debug bool
	flag.BoolVar(&debug, "debug", false, "Print debugging info")

	flag.Parse()
	baseDir := goof.ExecutablePath()
	os.Chdir(baseDir)
	c := LoadConfig()
	LoadInfo()

	var err error
	p2pnet, err = p2p.NewNetwork(fmt.Sprintf(":%v", c.PeerPort), []byte(c.PreSharedKey), []byte(c.NetworkPassword))
	if err != nil {
		panic(err)
	}

	p2p.GlobalDebug = debug
	p2pnet.NodeName = Info.Name

	fmt.Printf("Program path: %v\n", baseDir)

	p2pnet.ConfigBaseDir = baseDir
	os.Chdir(baseDir)
	//go ScanAll()

	for _, host := range Configuration.KnownPeers {
		p2pnet.AddEndpoint(host)
	}

	go p2pnet.Start()

	go startLocalServer(c.StartPagePort)

	onExit := func() {
		// now := time.Now()
		// ioutil.WriteFile(fmt.Sprintf(`on_exit_%d.txt`, now.UnixNano()), []byte(now.String()), 0644)
		fmt.Println("done")
	}

	for {
		systray.Run(onReady, onExit)
		log.Println("Reloading")
	}
}

var netEntitiesLock sync.Mutex

func AddSub(m *menu.Node, parent *systray.MenuItem) {
	// fmt.Printf("*****%+v, %v\n", m.SubNodes, m)

	for _, v := range m.SubNodes {
		if len(v.SubNodes) > 0 {
			p := parent.AddSubMenuItem(fmt.Sprintf("%v", v.Name), "")
			AddSub(v, p)
		} else {
			// fmt.Printf("Adding submenu item \"%+v\"\n", v)
			p := parent.AddSubMenuItem(fmt.Sprintf("%v", v.Name), "")
			go func(v *menu.Node, p *systray.MenuItem) {
				for {
					<-p.ClickedCh
					fmt.Println("Clicked2", v.Name)
					fmt.Println("Clicked2", v.Command)
					menu.Activate(v.Command)
				}
			}(v, p)
		}
	}
}

func addTopLevelMenuItems(m *menu.Node) {
	// AddSub(apps, appMen)
	for _, v := range m.SubNodes {
		p := systray.AddMenuItem(fmt.Sprintf("%v", v.Name), "")
		go func(v *menu.Node) {
			for {
				<-p.ClickedCh
				fmt.Println("Clicked top level", v.Name)
				menu.Activate(v.Command)

			}
		}(v)
		if len(v.SubNodes) > 0 {
			fmt.Println("Adding submenu ", v.Name)
			AddSub(v, p)
		} else {
			fmt.Println("Adding menu item", v.Name)
		}

	}
}

func RecallMenu() *menu.Node {
	m := menu.Recall()
	out := menu.MakeNodeLong("Recall", []*menu.Node{}, "", "")
	for _, entry := range m {
		h := menu.MakeNodeLong(entry[0], []*menu.Node{}, entry[1], "")
		out.SubNodes = append(out.SubNodes, h)
	}
	return out
}

func trim(s string) string {
	out := strings.TrimSpace(s)
	return out
}

func listWifi() []string {
	// Macosx
	str, _ := goof.QC([]string{"/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport", "scan"})
	wifi_str := trim(str)
	lines := strings.Split(wifi_str, "\n")
	out := []string{}
	for _, l := range lines {
		ll := trim(l)
		bits := strings.Split(ll, " ")
		out = append(out, bits[0])
	}
	return out
}

func makeWifiMenu(ssids []string) *menu.Node {
	out := menu.MakeNodeLong("Wifi", []*menu.Node{}, "", "")
	for _, network := range ssids {
		h := menu.MakeNodeLong(network, []*menu.Node{}, "shell://networksetup -setairportnetwork en0 \""+network+"\" password_goes_here", "")

		out.SubNodes = append(out.SubNodes, h)
	}
	return out
}
func MakeUserMenu() *menu.Node {
	var usermenu menu.Node
	b, err := ioutil.ReadFile("config/usermenu.json")
	if err != nil {
		log.Println("Error reading usermenu.json", err)
		return &usermenu
	}
	err = json.Unmarshal(b, &usermenu)
	if err != nil {
		log.Println("Error unmarshalling usermenu.json", err)
	}
	return &usermenu
}

func onReady() {
	m := UberMenu()

	netmenus := menu.Node{Name: "Network", SubNodes: []*menu.Node{}}

	fmt.Printf("%+v, %v\n", m.SubNodes, m)
	systray.AddMenuItem("UMH", fmt.Sprintf("Universal Menu, version %v", Version))

	var apps *menu.Node
	if runtime.GOOS == "darwin" {
		apps = menu.AppsMenu()
	} else {
		apps = menu.TieredAppsMenu()
	}
	m.SubNodes = append(m.SubNodes, apps)

	m.SubNodes = append(m.SubNodes, makeWifiMenu(listWifi()))

	usermenu := MakeUserMenu()
	m.SubNodes = append(m.SubNodes, usermenu)
	m.SubNodes = append(m.SubNodes, usermenu.SubNodes...)
	m.SubNodes = append(m.SubNodes, RecallMenu())
	addTopLevelMenuItems(m)

	mQuitOrig := systray.AddMenuItem("Reload", "Reload menu")
	go func() {
		<-mQuitOrig.ClickedCh

		goof.Restart()
		systray.Quit()
		fmt.Println("Systray stopped")
	}()

	// We can manipulate the systray in other goroutines
	go func() {
		systray.SetTemplateIcon(icon.Data, icon.Data)
		systray.SetTitle("UMH")
		systray.SetTooltip("Universal Menu")
		statusMenuItem := systray.AddMenuItem("-------------", " --------------")
		setStatus = func(status string) {
			statusMenuItem.SetTitle(status)
		}

		// mChecked := systray.AddMenuItem("Unchecked", "Check Me")

		mQuit := systray.AddMenuItem("退出", "Quit the whole app")

		// Sets the icon of a menu item. Only available on Mac.
		mQuit.SetIcon(icon.Data)
		netmenu2 = systray.AddMenuItem("Network", "Network")
		// Start the network scan after the menu is fully loaded, otherwise we can run out of file
		// handles and not be able to open the config files
		go func() {

			//netmenu, globalmenu := MakeNetworkPcMenu(frogpond.AllHosts())

			//netmenus.SubNodes = append(netmenus.SubNodes, netmenu)
			//netmenus.SubNodes = append(netmenus.SubNodes, globalmenu)
			addTopLevelMenuItems(&netmenus)
		}()

		for {
			select {
			case <-statusMenuItem.ClickedCh:
				statusMenuItem.SetTitle("----------------")
				statusMenuItem.SetIcon(icon.Data)
				/*
					case <-mChecked.ClickedCh:
						if mChecked.Checked() {
							mChecked.Uncheck()
							mChecked.SetTitle("Unchecked")
						} else {
							mChecked.Check()
							mChecked.SetTitle("Checked")
						}
				*/
			case <-mQuit.ClickedCh:
				fmt.Println("Requesting quit")
				systray.Quit()
				os.Exit(0)
			}
		}
	}()
}
