package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	//"github.com/donomii/menu"

	p2p "../p2p"
	"github.com/donomii/goof"
)

var noScan bool
var p2pnet *p2p.Network

type Config struct {
	HttpPort           uint
	StartPagePort      uint
	Name               string
	MaxUploadSize      uint
	Networks           []string
	KnownPeers         []string
	ArpCheckInterval   int
	PeerUpdateInterval int
	PreSharedKey       string
	NetworkPassword   string
	PeerPort		   uint
}

type Service struct {
	Name        string
	Ip          string
	Port        int
	Protocol    string
	Description string
	Global      bool
	Path        string
}


type InfoStruct struct {
	GUID     string
	Name     string
	Services []Service
}

var Info InfoStruct

var Configuration Config

func LoadConfig() Config{
	data, err := ioutil.ReadFile("config/p2p.json")
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &Configuration)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Loaded config from %v: %+v\n", "config/p2p.json", Configuration)
	return Configuration
}

func LoadInfo() {
	
	fmt.Println("Loading info")
	data, err := ioutil.ReadFile("config/public_info.json")
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &Info)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Loaded info: %+v\n", Info)
}

func main() {
	var debug bool
	flag.BoolVar(&debug, "debug", false, "Print debugging info")

	flag.Parse()
	baseDir := goof.ExecutablePath()
	os.Chdir(baseDir)
	c:=LoadConfig()
	LoadInfo()

	var err error
	p2pnet ,err= p2p.NewNetwork(fmt.Sprintf(":%v",c.PeerPort),[]byte(c.PreSharedKey ),[]byte(c.NetworkPassword))
	if err != nil {
		panic(err)
	}
	
	p2p.GlobalDebug = debug
	p2pnet.NodeName = Info.Name
	fmt.Printf("Program path: %v\n", baseDir)

	p2pnet.ConfigBaseDir = baseDir
	os.Chdir(baseDir)
	//go ScanAll()

	for _, host := range Configuration.KnownPeers {
		p2pnet.AddEndpoint(host)
	}
	
	p2pnet.Start()
	log.Printf("Trayheadless finished\n")
}
