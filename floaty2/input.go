package main

//Routines to handle mouse movement, keyboard, and window events

import (
	"fmt"
	"log"

	"github.com/donomii/goof"
	"github.com/go-gl/glfw/v3.3/glfw"
)

func doKeyPress(action string) {
	switch action {

	case "SelectPrevious":
		selected -= 1
		if selected < 0 {
			selected = 0
		}

	case "SelectNext":
		selected += 1
		if selected > len(pred)-1 {
			selected = len(pred) - 1
		}

	case "Backspace":
		if len(input) > 0 {
			input = input[0 : len(input)-1]
		}

	}

}

func handleKeys(window *glfw.Window, block *BlockConfig) {
	EscapeKeyCode := 256
	MacEscapeKeyCode := 53
	MacF12KeyCode := 301

	window.SetKeyCallback(func(w *glfw.Window, key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {

		log.Printf("Got key %c,%v,%v,%v", key, key, mods, action)

		/*if key == 301 {
			hideWindow()
			return
		}
		*/
		if action > 0 {
			if key == glfw.Key(MacF12KeyCode) || key == 109 {
				doKeyPress("HideWindow")
			}
			//ESC
			if key == glfw.Key(EscapeKeyCode) || key == glfw.Key(MacEscapeKeyCode) {
				//os.Exit(0)
				log.Println("Escape pressed")
				wantExit = true
				doKeyPress("HideWindow")
				return
			}

			if key == 265 {
				doKeyPress("SelectPrevious")
			}

			if key == 264 {
				doKeyPress("SelectNext")
			}

			if key == 257 {
				doKeyPress("Activate")
			}

			if key == 259 {
				doKeyPress("Backspace")
			}

			UpdateBuffer(block.Editor, input)
			update = true
		}

	})

	window.SetCharModsCallback(func(w *glfw.Window, char rune, mods glfw.ModifierKey) {

		text := fmt.Sprintf("%c", char)
		input = input + text
		UpdateBuffer(block.Editor, input)
		update = true

	})
}

func handleFocus(window *glfw.Window) {

	window.SetFocusCallback(func(w *glfw.Window, focused bool) {
		log.Printf("Focus changed to %v\n", focused)
		if !focused {
			invalidCoords = true
			log.Println("Marked coords as invalid")
		}
	})
}
func handleMouse(window *glfw.Window) {
	window.SetMouseButtonCallback(func(w *glfw.Window, button glfw.MouseButton, action glfw.Action, mods glfw.ModifierKey) {

		if button == glfw.MouseButtonLeft {
			if action == glfw.Press {
				log.Println("Mouse button pressed")
				if invalidCoords {
					mouseX, mouseY = window.GetCursorPos()
					lastMouseX = mouseX
					lastMouseY = mouseY

				}

				if !mouseDrag {
					dragStartX = globalMouseX
					dragStartY = globalMouseY

					origPosX, origPosY = window.GetPos()
					dragOffSetX = mouseX
					dragOffSetY = mouseY

					mouseDrag = true
				}
			}

			if action == glfw.Release {
				deltaX := globalMouseX - dragStartX
				deltaY := globalMouseY - dragStartY
				if goof.AbsFloat64(deltaX) < 10 && goof.AbsFloat64(deltaY) < 10 {
					fmt.Println("click!")
					conf.Text, _ = goof.QC(command)
				}
				mouseDrag = false
			}
		}

		if button == glfw.MouseButtonRight {
			if action == glfw.Release {

			}
		}
	})
}

func handleMouseMove(window *glfw.Window, block *BlockConfig) {

	window.SetCursorPosCallback(func(w *glfw.Window, xpos float64, ypos float64) {
		log.Printf("Block %v\n", block.ID)
		log.Printf("Mouse moved to %v,%v\n", xpos, ypos)
		if invalidCoords {
			lastMouseX = xpos
			lastMouseY = ypos
		} else {
			lastMouseX = mouseX
			lastMouseY = mouseY
		}
		mouseX = xpos
		mouseY = ypos
		X, Y := window.GetPos()
		globalMouseX = xpos + float64(X)
		globalMouseY = ypos + float64(Y)

		log.Printf("Mouse moved to %v,%v, last X,Y: %v,%v\n", globalMouseX, globalMouseY, lastMouseX, lastMouseY)
		if mouseDrag && !invalidCoords {

			deltaX := globalMouseX - dragStartX
			deltaY := globalMouseY - dragStartY
			log.Printf("deltaX: %v, deltaY: %v, mouseX: %v, mouseY: %v, dragStartX: %v, dragStartY: %v, dragOffSetX: %v, dragOffSetY: %v\n", deltaX, deltaY, mouseX, mouseY, dragStartX, dragStartY, dragOffSetX, dragOffSetY)
			log.Printf("globalMouseX: %v, globalMouseY: %v\n", globalMouseX, globalMouseY)
			if (deltaX > 10) || (deltaX < -10) || (deltaY > 10) || (deltaY < -10) {
				block.WindowX = int(globalMouseX)
				block.WindowY = int(globalMouseY)
				log.Printf("Dragged to %v,%v\n", block.WindowX, block.WindowY)

				update = true
			}
		}

		invalidCoords = false
		log.Println("Marked coords as valid")

	})
}
