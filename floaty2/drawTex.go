package main

//Draw pictures and words onto a texture
import (
	"fmt"
	"image"
	"io"
	"log"
	"math/rand"
	"os"
	"runtime/debug"

	"github.com/srwiley/oksvg"
	"github.com/srwiley/rasterx"

	"github.com/donomii/glim"
)

var foreColour, backColour *glim.RGBA

// Draw a picture and text onto a byte array, which is used as a texture for OpenGL
func renderEd(block *BlockConfig,  targetWidth, targetHeight int) {
	defer func() {
		if r := recover(); r != nil {
			debug.PrintStack()
			fmt.Println("Recovered in renderEd", r)

		}
	}()
	left := 0
	top := 0

	if block.Editor != nil {
		log.Println("Starting editor draw")
		ClearActiveBuffer(block.Editor)
		ActiveBufferAppend(block.Editor,block.Text)

		size := targetWidth * targetHeight * 4
		log.Println("Clearing", size, "bytes(", targetWidth, "x", targetHeight, ")")
		backColour = &glim.RGBA{0, 0, 0, 0}
		if picture_path != "" {
			for i := 0; i < size; i = i + 1 {
				block.Texture[i] = 0
			}
			background_picture, background_pictureX, background_pictureY := glim.LoadImage(picture_path)
			for y := 0; y < background_pictureY; y++ {
				for x := 0; x < background_pictureX; x++ {
					for rgba := 0; rgba < 4; rgba++ {
						bp := y*background_pictureX*4 + x*4 + rgba
						i := y*block.EditorWidth*4 + x*4 + rgba
						//log.Printf("x,y: %v,%v.  bpX,bpY: %v,%v. edWidth,edHeight: %v,%v.  i, bpi: %v,%v\n", x, y, background_pictureX, background_pictureY, edWidth, edHeight, i, bp)
						block.Texture[i] = background_picture[bp]
					}
				}
			}
		} else {
			if block.Svg_path != "" {

				background_pictureY, background_pictureX := targetWidth, targetHeight
				var err error
				var in io.ReadCloser
				if conf.Internal {
					fmt.Println("Loading internal", block.Svg_path)
					in, err = embeddedFS.Open(block.Svg_path)
					if err != nil {
						panic(err)
					}
					defer in.Close()
				} else {
					in, err = os.Open(block.Svg_path)
					if err != nil {
						panic(err)
					}
					defer in.Close()
				}
				icon, err := oksvg.ReadIconStream(in)
				if err != nil {
					panic(err)
				}
				icon.SetTarget(0, 0, float64(targetWidth), float64(targetHeight))
				rgba_image := image.NewRGBA(image.Rect(0, 0, targetWidth, targetHeight))

				ras := rasterx.NewDasher(targetWidth, targetHeight, rasterx.NewScannerGV(targetWidth, targetHeight, rgba_image, rgba_image.Bounds()))
				icon.Draw(ras, 1)

			
				//fmt.Printf("Source size: %v,%v target size: %v,%v\n", background_pictureX, background_pictureY, targetWidth, targetHeight)
				pic, _, _ := glim.GFormatToImage(rgba_image, nil, targetWidth, targetHeight)
				for y := 0; y < background_pictureY; y++ {
					for x := 0; x < background_pictureX; x++ {
						backgroundpixel_index := y*background_pictureX*4 + x*4

						if pic[backgroundpixel_index+0]+pic[backgroundpixel_index+1]+pic[backgroundpixel_index+2]+pic[backgroundpixel_index+3] > 1 {
							fmt.Print("*")
						} else {
							fmt.Print("_")
						}
						for rgba := 0; rgba < 4; rgba++ {

							bp := backgroundpixel_index + rgba
							i := y*targetWidth*4 + x*4 + rgba

							if rgba != 3 {
								block.Texture[i] = pic[backgroundpixel_index+3]
								continue
							}
							//log.Printf("x,y: %v,%v.  bpX,bpY: %v,%v. edWidth,edHeight: %v,%v.  i, bpi: %v,%v\n", x, y, background_pictureX, background_pictureY, edWidth, edHeight, i, bp)
							block.Texture[i] = pic[bp]
						}
					}
					fmt.Println()
				}
				

			} else {

				patternColour := &glim.RGBA{128, 100, 100, 255}
				foreColour = &glim.RGBA{255, 255, 255, 255}
				num := rand.Intn(100)
				log.Println("Background rand:", num)
				for i := 0; i < size; i = i + 4 {
					var Colour *glim.RGBA
					if (i^int(i/targetWidth))%3 == 0 {
						//if !(int(math.Pow(float64(i), float64(int(i/w))))%9>0){
						Colour = patternColour
					} else {
						Colour = backColour
					}
					block.Texture[i] = ((*Colour)[0])
					block.Texture[i+1] = ((*Colour)[1])
					block.Texture[i+2] = ((*Colour)[2])
					block.Texture[i+3] = ((*Colour)[3])
				}

			}

		} 

		form = block.Editor.ActiveBuffer.Formatter
		form.Colour = foreColour
		form.Outline = true

		mouseX := 10
		mouseY := 10
		displayText := block.Editor.ActiveBuffer.Data.Text

		log.Println("Render paragraph", string(displayText))

		block.Editor.ActiveBuffer.Formatter.FontSize = fontSize
		glim.RenderPara(block.Editor.ActiveBuffer.Formatter,
			0, 0, 0, 0,
			targetWidth, targetHeight, targetWidth, targetHeight,
			int(mouseX)-left, int(mouseY)-top, block.Texture, displayText,
			false, true, false)
		log.Println("Finished render paragraph")
	}else {
		log.Printf("Skipping editor draw\n")
	}
}
