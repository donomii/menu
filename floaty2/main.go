package main

import (
	"fmt"
	"os"
	"runtime"
	"time"

	"github.com/donomii/glim"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"

	_ "embed"
)

//go:embed logo.png
var logo_bytes []byte

// Arrange that main.main runs on main thread.
func init() {
	runtime.LockOSThread()
}

type BlockConfig struct {
	WindowX, WindowY          int
	WinHandle                 *glfw.Window
	State                     *State
	Editor                    *GlobalConfig
	EditorWidth, EditorHeight int
	Texture                   []uint8
	NeedsUpdate               bool
	Width, Height             int
	Svg_path                  string
	Text                      string
	Blocks                    []*BlockConfig
	ID                        int
}

var blockSize = 32

func main() {
	fmt.Println("Starting...")
	startTime := time.Now()
	blocks := []*BlockConfig{}

	// Load command line options
	doCommandLine(conf)

	gfxStart()
	var picture []uint8
	//var pictureX, pictureY int
	if picture_path != "" {
		fmt.Println("Loading picture", picture_path)
		picture, _, _ = glim.LoadImage(picture_path)
	}

	for i := 0; i < len(svg_paths); i = i + 1 {
		blocks = append(blocks, &BlockConfig{})
		blocks[i].Text = fmt.Sprintf("Block %v", i)
		blocks[i].ID = i
		win, state := openWindow(blockSize, blockSize, fmt.Sprintf("Block %v", i), conf.WantTransparent)
		blocks[i].WinHandle = win
		blocks[i].State = state

		blocks[i].Texture = make([]uint8, 3000*3000*4)
		copy(blocks[i].Texture, picture)

		blocks[i].EditorWidth = blockSize
		blocks[i].EditorHeight = blockSize

		// Create a text formatter.  This controls the appearance of the text, e.g. colour, size, layout
		form := glim.NewFormatter()
		blocks[i].Editor = NewEditor()
		blocks[i].Editor.ActiveBuffer.Formatter = form
		SetFont(blocks[i].Editor.ActiveBuffer, fontSize)

		handleKeys(blocks[i].WinHandle, blocks[i])
		handleMouse(blocks[i].WinHandle)
		handleMouseMove(blocks[i].WinHandle, blocks[i])
		handleFocus(blocks[i].WinHandle)

		blocks[i].WinHandle.SetSizeCallback(func(w *glfw.Window, width int, height int) {
			blocks[i].Width = width
			blocks[i].Height = height

			blocks[i].NeedsUpdate = true
		})

		if conf.Lsys {
			blocks[0].State = lsys_init(blocks[i].State)
		}
		conf.WindowX, conf.WindowY = win.GetPos()
		blocks[i].WindowX, blocks[i].WindowY = win.GetPos()

	}

	blocks[0].Text = conf.Text

	for i := range blocks {
		blocks[i].Blocks = blocks
	}

	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	for {
		if conf.Lsys {
			update = true
		}
		for i := range blocks {
			if blocks[i].WinHandle.ShouldClose() {
				os.Exit(0)
			}
		}
		if update {
			for i, block := range blocks {
				if block.ID > 0 {
					blocks[i].WindowX = blocks[i-1].WindowX
					blocks[i].WindowY = blocks[i-1].WindowY + blockSize
				}
			}
			for i := range blocks {
				blocks[i].WinHandle.SetPos(int(blocks[i].WindowX), int(blocks[i].WindowY))
				if conf.Lsys {
					lsys_gfxMain(blocks[i].WinHandle, blocks[i].State)

				} else {

					texWidth := int(blockSize)
					texHeight := int(blockSize)

					blocks[i].WinHandle.MakeContextCurrent()
					gl.ActiveTexture(gl.TEXTURE0)
					gl.BindTexture(gl.TEXTURE_2D, blocks[i].State.Texture)

					UpdateBuffer(blocks[i].Editor, conf.Text)
					renderEd(blocks[i], blockSize, blockSize)
					logo := glim.FlipUp(texWidth, texHeight, blocks[i].Texture)
					gl.TexImage2D(
						gl.TEXTURE_2D, 0,
						gl.RGBA,
						int32(texWidth), int32(texHeight), 0,
						gl.RGBA,
						gl.UNSIGNED_BYTE, gl.Ptr(logo),
					)

				}

				if !conf.Lsys {
					gfxMain(blocks, blockSize, blockSize)
				}

			}
			update = false
		} else {
			time.Sleep(time.Millisecond * 50)
		}
		glfw.PollEvents()
		elapsedTime := time.Since(startTime)
		if wantTimeOut && elapsedTime.Seconds() > float64(timeOut) {
			shutdown()
			os.Exit(0)
		}
	}
	shutdown()
}

func shutdown() {
}
