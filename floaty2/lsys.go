package main

// Alternate draw system for lsystem icons.  It is completely self-contained, and contains all the shader setup and draw routines needed.  You only need to set up an OpenGL window and context, then call lsys_init and lsys_gfxMain.  Good luck getting the OpenGL library versions to match! hahahaha
import (
	"gitlab.com/donomii/wasm-lsystem/lsystem"
	"fmt"
	"log"
	"strings"

	"github.com/donomii/sceneCamera"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"
	"github.com/go-gl/mathgl/mgl32"
)

var currentTemplate = " HR s s s s s Arrow "

type immob struct {
	Location []float32
	Lsys     string
}

var (
	sceneList    []immob
	ObjectBank   []immob
	CurrentScene *lsystem.Scene
	scene_camera *sceneCamera.Camera = sceneCamera.New(1)
	UserPos                               = mgl32.Vec3{0, 0, -1.1}
)

// Sets up the shaders and VAO/VBO/CBO, as well as the actual lsystem
func lsys_init(state *State) *State {
	var err error
	// Configure the vertex and fragment shaders
	state.Program, err = newLsysProgram(lsys_vertexShader, lsys_fragmentShader)
	if err != nil {
		panic(err)
	}

	// Activate the program we just created.  Will use the render and fragment shaders we compiled above
	gl.UseProgram(state.Program)

	state.Vao, state.Vbo, state.VertAttrib = make_array_buffer("vert", 3, state.Program, gl.FLOAT)
	state.Cao, state.Cbo, state.ColourAttrib = make_array_buffer("s_col", 4, state.Program, gl.FLOAT)
	gl.BindFragDataLocation(state.Program, 0, gl.Str("outputColor\x00"))
	resetCam(scene_camera)

	// Create the lsystems
	sceneLibList := lsystem.InitScenes(scene_camera)
	CurrentScene = sceneLibList[0]
	CurrentScene.Init(CurrentScene)

	ObjectBank = append(ObjectBank, immob{[]float32{0, 0, 0}, "HR S S Arrow"})
	ObjectBank = append(ObjectBank, immob{[]float32{0, 0, 0}, "HR S S Arrow"})
	ObjectBank = append(ObjectBank, immob{Lsys: "HR leaf", Location: []float32{0, 0, 0}})
	ObjectBank = append(ObjectBank, immob{Lsys: "p p p S HR Flower", Location: []float32{0, 0, 0}})
	ObjectBank = append(ObjectBank, immob{Lsys: "HR  starburst", Location: []float32{0, 0, 0}})
	ObjectBank = append(ObjectBank, immob{Lsys: "HR S Icosahedron", Location: []float32{0, 0, 0}})
	ObjectBank = append(ObjectBank, immob{Lsys: "HR Tetrahedron", Location: []float32{0, 0, 0}})

	// sceneList = append(sceneList, immob{Lsys: "HR  Icosahedron ", Location: []float32{0, 0, 0}})
	// sceneList = append(sceneList, immob{Lsys: "HR  starburst", Location: []float32{0, 0, 0}})
	// sceneList = append(sceneList, immob{Lsys: "HR leaf", Location: []float32{0, 0, 0}})
	// sceneList = append(sceneList, immob{Lsys: "p p p S HR Flower", Location: []float32{0, 0, 0}})
	sceneList = append(sceneList, ObjectBank[conf.LsysId])

	return state
}

// Draws a frame
func lsys_gfxMain(win *glfw.Window, state *State) {
	now := glfw.GetTime()
	elapsed := now - state.PreviousTime

	// Configure global settings

	gl.UseProgram(state.Program)
	gl.ClearColor(0.0, 0.0, 0.0, 0.0)

	gl.Disable(gl.BLEND)
	gl.Enable(gl.DEPTH_TEST)

	gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	scene_camera.SetPosition(UserPos.X(), UserPos.Y(), UserPos.Z())

	if elapsed > 0.050 && 1 != win.GetAttrib(glfw.Iconified) {
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		angle := state.Angle
		angle += elapsed
		state.Angle = angle

		vertices, colours := calcLsys()

		projection := mgl32.Perspective(mgl32.DegToRad(45.0), 1.0, 0.1, 1000.0)
		// cam := mgl32.LookAtV(mgl32.Vec3{0, 0, 0}, mgl32.Vec3{0, 0, 0}, mgl32.Vec3{0, 1, 0})
		// camera := scene_camera.ViewMatrix()//.Mul4(projection) //mgl32.Ident4() //cam.Mul4(projection)

		// FIXME
		// Actually, fix the scenecam library
		m := scene_camera.ViewMatrix()
		m = mgl32.Translate3D(0, 0, -10.0).Mul4(m)
		camera := projection.Mul4(m)
		// log.Println("mvp", camera)
		cameraUniform := gl.GetUniformLocation(state.Program, gl.Str("MVP\x00"))
		checkGlError()
		//	mvp := scene_camera.ViewMatrix()
		gl.UniformMatrix4fv(cameraUniform, 1, false, &camera[0])
		checkGlError()

		// fmt.Printf("array len: %v\n", len(colours))

		gl.BindVertexArray(state.Vao)
		checkGlError()
		gl.BindBuffer(gl.ARRAY_BUFFER, state.Vbo)
		checkGlError()
		gl.BufferData(gl.ARRAY_BUFFER, len(vertices)*4, gl.Ptr(vertices), gl.STATIC_DRAW)
		// log.Println(vertices)
		checkGlError()
		gl.VertexAttribPointer(uint32(state.VertAttrib), 3, gl.FLOAT, false, 0, gl.PtrOffset(0))
		checkGlError()
		gl.EnableVertexAttribArray(uint32(state.VertAttrib))
		checkGlError()

		gl.BindBuffer(gl.ARRAY_BUFFER, state.Cbo)
		checkGlError()
		gl.BufferData(gl.ARRAY_BUFFER, len(colours)*4, gl.Ptr(colours), gl.STATIC_DRAW)
		checkGlError()
		gl.VertexAttribPointer(uint32(state.ColourAttrib), 4, gl.FLOAT, false, 0, gl.PtrOffset(0))
		checkGlError()
		gl.EnableVertexAttribArray(uint32(state.ColourAttrib))
		checkGlError()

		gl.BindVertexArray(state.Vao)
		checkGlError()
		gl.DrawArrays(gl.TRIANGLES, 0, int32(len(vertices)/3))
		checkGlError()

		win.SwapBuffers()

	}
}

// Creatges an openGL program from the given vertex and fragment shaders
func newLsysProgram(lsys_vertexShaderSource, lsys_fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(lsys_vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		log.Println(err)
		return 0, err
	}

	fragmentShader, err := compileShader(lsys_fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		log.Println(err)
		return 0, err
	}

	program := gl.CreateProgram()
	// This is the variable in the fragment shader that will hold the colour for each pixel

	checkGlError()
	gl.AttachShader(program, vertexShader)
	checkGlError()
	gl.AttachShader(program, fragmentShader)
	checkGlError()
	gl.LinkProgram(program)
	checkGlError()

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}

	gl.DeleteShader(vertexShader)
	checkGlError()
	gl.DeleteShader(fragmentShader)
	checkGlError()

	return program, nil
}

// Simple Model-View-Projection matrix shader
var lsys_vertexShader = `
#version 330
layout(location = 1) in vec4 s_col;
layout(location = 2) in vec3 vert;
 uniform  mat4 MVP;
out  vec4 fragCol;

void main() {
    gl_Position = MVP*vec4(vert, 1);
	fragCol = s_col;
	fragCol.a=1.0;
	//fragCol =  vec4(1.0,0.0,0.0,1.0)+s_col;
}
` + "\x00"

// Super simple fragment shader
var lsys_fragmentShader = `
#version 330
in vec4 fragCol;
layout(location = 0) out  vec4 outputColor;
void main() {
	outputColor = fragCol;
}
` + "\x00"

// Create the VAOs and VBOs for the lsystem
func make_array_buffer(name string, components int32, program uint32, tpe uint32) (uint32, uint32, int32) {
	var Vao, Vbo uint32
	var attrib int32

	gl.GenVertexArrays(1, &Vao)
	gl.BindVertexArray(Vao)

	gl.GenBuffers(1, &Vbo)
	checkGlError()
	gl.BindBuffer(gl.ARRAY_BUFFER, Vbo)
	checkGlError()
	attrib = int32(gl.GetAttribLocation(program, gl.Str(name+"\x00")))
	if attrib < 0 {
		log.Printf("Could not find attribute %v, any operations on this attribute will fail\n", name)
	}
	checkGlError()

	gl.VertexAttribPointer(uint32(attrib), components, tpe, false, 0, gl.PtrOffset(0))
	checkGlError()

	return Vao, Vbo, attrib
}

// Draw the lsystem.  Returns an array of vertices for triangles, and an array of colours for those vertices.
func calcLsys() ([]float32, []float32) {
	movMatrix := mgl32.Ident4()
	// movMatrix = Move(movMatrix, 1.0, 0.0, 0.0)

	verticesNative, colorsNative,_ := lsystem.Draw(CurrentScene,  lsystem.S(`
			Colour254,254,254 
		
			deg30 s s s s  r r F p p p f f f f [ HR
				s s s s
				[ s s HR Icosahedron ] TF TF TF TF 
				
	
				[ p p p s s HR leaf ] Arrow  F  Arrow  F  Arrow  F 	
				
				[ p p p s s s HR lineStar ] TF TF TF
				[  ] TF TF TF
				[ p p p s s HR Flower12 ] TF TF TF
				[ p p p s s HR Flower11 ] TF TF TF
				[ p p p s s HR Flower10 ] TF TF TF
				
				
			]
			
			p p p F P P P
			[ s s s s
			
				
				[ p p p S S S HR Square1 ] TF TF TF
				[ p p p S S S S S S HR Face ] TF TF TF
				[ p p p S S S HR Arrow ] TF TF TF
				[ p p p S HR Prism ] TF TF TF
				[ p p p S HR Prism1 ] TF TF TF
				[   s s HR p p p Circle ] TF TF TF

				
			]
			
			`), movMatrix, true)

	for _, imm := range sceneList {
		movMatrix := mgl32.Ident4()
		movMatrix = Move(movMatrix, imm.Location[0], imm.Location[1], imm.Location[2])

		a, b,_ := lsystem.Draw(CurrentScene,  lsystem.S(imm.Lsys), movMatrix, true)
		verticesNative = append(verticesNative, a...)
		colorsNative = append(colorsNative, b...)
	}

	/*
		//This adds a lsys object under the mouse cursor.  Useful for interacting with a game scene
		movMatrix = mgl32.Ident4()
		movMatrix = Move(movMatrix, x, y, 0)

		a, b := lsystem.Draw(CurrentScene, scene_camera.ViewMatrix(), lsystem.S(currentTemplate), movMatrix, true)
		verticesNative = append(verticesNative, a...)
		colorsNative = append(colorsNative, b...)
	*/
	return verticesNative, colorsNative
}

// Translate a matrix by a given amount
func Move(movMatrix mgl32.Mat4, x, y, z float32) mgl32.Mat4 {
	movMatrix = movMatrix.Mul4(mgl32.Translate3D(x, y, z))
	return movMatrix
}
