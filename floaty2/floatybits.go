package main

// Lots of global variables, command line options, etc
import (
	"embed"
	"flag"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/donomii/glim"
)

// All the config needed for the floating icon
type Config struct {
	RetinaMode bool    // Are we running on a apple mac retina display?
	AutoRetina bool    // Attempt to auto-detect a retina display
	FontSize   float64 // Font size

	WantTransparent  bool   // Should we respect alpha values in the picture?
	Internal         bool   // Load icon from the internal icon store
	WindowX, WindowY int    // Window position on desktop
	Text             string // Initial text
	Lsys             bool   // Display a lsystem?
	LsysId           int    // Id of the built-in lsystem to display
}

var conf = &Config{
	RetinaMode:      false,
	AutoRetina:      true,
	FontSize:        float64(16),
	WantTransparent: false,
	Internal:        false,
	WindowX:         100,
	WindowY:         100,

	Lsys: false,
}

//go:embed internal_icons
var embeddedFS embed.FS

var (
	confFile           string   // The config file path
	origPosX, origPosY int      // drag start position
	pred               []string // Text prediction
	predAction         []string
	input, status      string
	selected           int
	update             bool = true // Need to redraw scene
	form               *glim.FormatParams
	mode               = "searching"
	watchPid           int
)

var (
	wantWindow                                                                                   = true
	createWin                                                                                    = true
	glfwInitialized                                                                              = false
	mouseX, mouseY, lastMouseX, lastMouseY, dragOffSetX, dragOffSetY, globalMouseX, globalMouseY float64
	dragStartX, dragStartY                                                                       float64
	mouseDrag                                                                                    = false
)

var wantExit = false

var (
	command       = []string{"osascript", "-e", "tell app \"Terminal\" to do script \"echo hello\""}
	fontSize      = float64(16)
	captureOutput = false
	picture_path  string
	svg_paths     []string
)

var invalidCoords = true
var timeOut = 0
var wantTimeOut = false

func doCommandLine(conf *Config) {
	var doLogs, noAutoRetina, listInternal bool
	var svg_path string
	flag.BoolVar(&doLogs, "debug", false, "Display debug information")
	flag.BoolVar(&captureOutput, "capture-output", false, "Capture output of click command")
	flag.StringVar(&conf.Text, "text", conf.Text, "Initial text")
	//flag.StringVar(&picture_path, "picture", "", "PNG, JPEG or GIF to display on tile")
	flag.StringVar(&svg_path, "svg", "", "SVG file to display on tile")
	flag.Float64Var(&fontSize, "font-size", fontSize, "Font size")
	flag.BoolVar(&conf.WantTransparent, "transparent", conf.WantTransparent, "Respect alpha values in picture")
	flag.BoolVar(&noAutoRetina, "no-auto-retina", noAutoRetina, "Switch off automatic retina scaling")
	flag.BoolVar(&conf.RetinaMode, "retina", conf.RetinaMode, "Scale correctly for retina displays.  Requires -no-auto-retina")
	flag.BoolVar(&conf.Internal, "internal", conf.Internal, "Load icon from internal store")
	flag.BoolVar(&listInternal, "list-internal", false, "List bundled icons")
	flag.BoolVar(&conf.Lsys, "lsys", conf.Lsys, "Display an lsystem")
	flag.IntVar(&conf.LsysId, "lsys-id", conf.LsysId, "Id of the built-in lsystem to display")
	flag.IntVar(&watchPid, "watch-pid", watchPid, "Pid to watch for termination")
	flag.IntVar(&blockSize, "size", blockSize, "Window size")
	flag.IntVar(&timeOut, "timeout", timeOut, "Timeout in seconds")
	flag.Parse()
	if timeOut > 0 {
		wantTimeOut = true
	}
	svg_paths = strings.Split(svg_path, ",")
	c := flag.Args()
	if listInternal {
		fs.WalkDir(embeddedFS, ".", func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(path)
			return nil
		})
		os.Exit(0)
	}

	go func() {
		if watchPid != 0 {
			_, err := os.FindProcess(watchPid)
			for err == nil {
				_, err = os.FindProcess(watchPid)
			}
			wantExit = true
		}
	}()

	if len(c) == 0 {
		log.Println("No command specified, using default")
		log.Println(`Command example: ./floaty osascript -e "tell app \"Terminal\" to do script \"cd ~/git/menu/floaty && go build .\""`)
	} else {
		command = c
		log.Printf("Command: %v\n", command)
	}

	if !doLogs {
		log.SetFlags(0)
		log.SetOutput(ioutil.Discard)
	}
}

func UpdateBuffer(ed *GlobalConfig, input string) {
	ClearActiveBuffer(ed)

	ActiveBufferInsert(ed, "\n")
	ActiveBufferInsert(ed, input)
	ActiveBufferInsert(ed, "\n\n")
}
