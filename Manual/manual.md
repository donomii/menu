# UMH

A cross platform desk set for modern computers.  It has a system tray menu, a popup search bar, and floating icons.

Every part of UMH is customisable.  You can set your own icons, and add your own choices to the menu and search bar.

UMH is open source.  If you don't like what it does, you can change it to do exactly what you like.

## Adding the missing features to desktops

UMH is a project to add the missing desktop features that I want.  Everytime I thought "Why can't I just ...", I put it into UMH.

UMH started when I thought "Why can't I just add my own menu to the screen?" and kept going from there.  

There are now three main components:  UMH menu, which puts a user-defined menu into the system tray(or the top menu bar on mac).  UMH launcher, which is a search bar launched by a hotkey (F12), and UMH floatie, which is an icon that stays on your screen, and launches a command when clicked.

All of these existed before, in other projects, but when I went looking for them recently I couldn't find anything.  

But recently, desktops having been getting much worse.  Spotlight on MacOSX slows down my computer trying to index the millions of source code files on my drive, so I turn it off.  The windows Start Menu has advertisements, so I never open it.  And Linux is, well, Linux.

## Work in progress

UMH is still a very young project, full of bugs and missing features.  Anything you want to contribute is appreciated, but patches and pull requests are the best.

You can find the source code to UMH at https://gitlab.com/donomii/menu

UMH contains a lot of features, but they aren't fully integrated, so there are multiple configuration files in different directories.

The code is a mix of many of my old projects that don't always fit together, so often some parts of it don't look like they fit properly, because they were just whatever I had lying around.  Better /cross platform/ replacements would be nice.

## Use

Most of the features of UMH are available from the menu.  Run ```universal_menu_main.exe``` and a menu should appear in your system tray or menu area, depending on OS.

The menu includes items like ```Launcher```, which starts the popup search bar/launcher, and ```Edit this menu``` which should open a text editor on the menu configuration file.

The exception to this is UMH Floaty, which works entirely from the command line.  The options for Floaty are discussed below.


### Command format

UMH uses urls where ever possible, and adds its own extensions.  These URLs can be used in the UMH menu configuration file, and in the ~/.menu.recall.text file.

- http://,https:// - Will open a web browser to this url
- shell:// - will run a shell command exactly as written.  The shell will be cmd.exe or bash.
- exec:// - attempt to run this program.  You can add simple arguments after the program name, as if you were in the shell (but in a really dumb shell)
- file:// - Open the file for display or editing using the default program
- internal:// - There are several internal commands provided, e.g. RunAtStartup
- clipboard:// - Copy the rest of this text to the clipboard(minus the clipboard:// part).  You probably shouldn't put your passwords here, but I do anyway

You can also substitute in a few variables using the Go template syntax e.g. {{.AppDir}} will be replaced with the program directory.

	{{.AppDir}}      The directory containing the UMH program file
	{{.ConfigDir}}   The config and data directory, usually "~/.umh"
	{{.Cwd}}         The current operations directory, as stored in "~/.umh/cwd"
	{{.Command}}     The command that is currently being run

### Configuring UMH Menu

UMH Menu stores its menu in userconfig.json.  You can edit this file from the menu option ```Edit this menu```.

The menu file contains many useful examples.  It is a proper JSON file, so any text editor that understands JSON will edit it correctly.

### Configuring UMH Launcher

UMH Launcher is a popup search bar that lets you add your own search results.  The recall file holds your custom results.  You can always find this file to edit by typing "Edit Recall Menu" into the Launcher, and then selecting the option that appears.

The file format looks like this

	Some text that you can search for | url://something that should happen

see "Command Format" for more details.

### Configuring Floaty

Floaty is currently configured entirely from the command line.  Config file support will be added later.

Running Floaty without any options will start a demonstration icon, which has an extremely boring icon and doesn't do much.  You can customise Floaty to your liking with command options, which are available with ```floaty --help```



