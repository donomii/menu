# UMH

Well, this got a little bit out of control.

A cross-platform system tray menu, that turned into a popup launcher and more.

There are currently three programs available: a system tray menu, a floating icon, and a popup launcher.

# Features

UMH has picked up a lot of different abilities.  As well as displaying a user menu, it can scan your network for other machines and services, publish services of its own, launch applications, open files to edit, open urls and run shell commands.

# Tray menu

The tray menu sits in the system tray(Windows) or the menu bar (Mac).  It displays the user-defined menu, as well as several auto-generated menus, like the Applications menu, and the network services menu.

# Floating icon

Creates an icon on the screen that floats in front of all your other windows.  You can choose what happens when you click it.

# Popup launcher

The popup launcher works similar to the built-in search on macosx.  Press a button (F12), and the launcher will appear.  Type your search, then use the up and down arrows to select your choice.

The options that appear in the popup launcher are not all automatically generated, they come from a file (~/.menu.recall.text).  You can always find this file by searching for "Edit Recall Menu", then selecting that option from the list.  


# Use

For more information on use, please consult the manual